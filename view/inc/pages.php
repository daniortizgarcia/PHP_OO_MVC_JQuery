<!-- Main Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
		<?php
			if (!$_GET) {
				$_GET['page'] = "controller_home";
				$_GET['op'] = "list";
			}
			switch($_GET['page']){
				case "controller_home":
					include("module/home/controller/".$_GET['page'].".php");
					break;
				case "controller_shoes":
					include("module/shoes/controller/".$_GET['page'].".php");
					break;
				case "controller_order":
					include("module/order/controller/".$_GET['page'].".php");
					break;
				case "news":
					include("module/news/".$_GET['page'].".php");
					break;
				case "controller_contact":
					include("module/contact/controller/".$_GET['page'].".php");
					break;
				case "controller_shop":
					include("module/shop/controller/".$_GET['page'].".php");
					break;
				case "controller_cart":
					include("module/cart/controller/".$_GET['page'].".php");
					break;
				case "controller_login":
					include("module/login/controller/".$_GET['page'].".php");
					break;
				case "controller_profile":
					include("module/profile/controller/".$_GET['page'].".php");
					break;
				case "404":
					include("view/inc/error".$_GET['page'].".php");
					break;
				case "503":
					include("view/inc/error".$_GET['page'].".php");
					break;
				default;
					include("view/inc/error404.php");
					break;
			}		
		?>       

        </div>
      </div>
    </div>

    <hr>