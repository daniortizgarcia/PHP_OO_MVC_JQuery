<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
	<div class="container">
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
		  Menu
		  <i class="fa fa-bars"></i>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
		  <ul class="navbar-nav ml-auto">
		    <li class="nav-item">
		     	<a class="nav-link" href="index.php?page=controller_home&op=list" data-tr="Inicio"></a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" href="index.php?page=controller_order&op=list" data-tr="Ordenar"></a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" href="index.php?page=news" data-tr="Noticias"></a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" href="index.php?page=controller_contact&op=list" data-tr="Contactanos"></a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" href="index.php?page=controller_shop&op=list_shop" data-tr="Tienda"></a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" href="index.php?page=controller_profile&op=list_profile" data-tr="Perfil"></a>
		    </li>
		    <li cla
		    <li class="nav-item">
		    	<a class="nav-link" href="index.php?page=controller_login&op=logout" onclick="DelCart()">LogOut</a>
		    </li>
		    <li class="nav-item" id="linkcart">
		    	<a class="nav-link" href="index.php?page=controller_cart&op=list_cart"><img id='carritob' src='view/img/carrisata.svg' style="display: none;"><img id='carritog' src='view/img/carrisatag.svg'><span id='numcarr'></span></a>
		    </li>
		  </ul>
		</div>
	</div>
</nav>
