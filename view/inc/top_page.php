<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Football shoes</title>
     <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="view/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="view/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Nunito|Roboto:300,400" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="view/css/clean-blog.min.css" rel="stylesheet">

    <!-- Mis css-->
    <link href="view/css/style.css" rel="stylesheet" type="text/css" />

    <!-- Mis links -->
	<script type="text/javascript" src="model/translate.js"></script>
    <script type="text/javascript" src="module/home/model/home.js"></script>
    <script type="text/javascript" src="module/login/model/validate_login.js"></script>
    <script type="text/javascript" src="module/login/model/validate_recpass.js"></script>
    <script type="text/javascript" src="model/activity.js"></script>
    <script type="text/javascript" src="module/news/news.js"></script>
    <script type="text/javascript" src="module/shop/model/shop.js"></script>
    <script type="text/javascript" src="module/cart/model/cart.js"></script>
    <script type="text/javascript" src="module/profile/model/profile.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script src="view/vendor/jquery/jquery.min.js"></script>
    <script src="view/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="view/js/clean-blog.min.js"></script>


  </head>

  <body>