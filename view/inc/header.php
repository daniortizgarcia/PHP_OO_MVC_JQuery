<!-- Page Header -->
<header class="masthead" style="background-image: url('view/img/header5.jpg')">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="site-heading">
          <div style="width: 18%; display: inline-block;"><img src="view/img/limshoes.svg" style="width: 100%; vertical-align: inherit;"></div>
          <h1 style="width:35%; display: inline-block; font-size: 55px !important;">LimShoes</h1>
        </div>
      </div>
    </div>
  </div>
</header>