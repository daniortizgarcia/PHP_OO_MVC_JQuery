HOME(ADMIN,USER,VISITOR)
Slider, Dummies, Dropdown, autocomplet

SHOES(ADMIN)
Create y update el formulario(radiobutton,checkbox) creados con bucle para ahorrar lineas de código
Modal en el Read
En el delete sale la id del producto, marca y modelo

ORDER(ADMIN,USER)
Data table de jqwidgets con varias opciones como paginar, elegir numero de filas a mostrar y buscar una palabra por campo

NEWS(ADMIN,USER)
Api de Marca y de L'equip, noticias deportivas, al pulsar la imagen redirige al articulo de la pagina oficial

CONTACTUS(ADMIN,USER,VISITOR)
Formulario de contacto con validación de php y js, envia a base de datos la petición del usuario
Google Maps con ubicación y descripción del lugar donde se encuentra la ubicación

SHOP(ADMIN,USER,VISITOR)
Tienda donde puedes comprar los productos creados desde el CRUD
Puedes elegir la cantidad de 1 a 20
Al pulsar la imagen se desplegá un details del producto
Al añadir al carrito solo se añade el código del producto y la cantidad, para hacer lo mas seguro posible la transacción

LOGIN(VISITOR)
A la hora de hacer el login y register hay validación
Recover password con validación 
La contraseña esta encriptada tanto como en el register como en el recover password
Detecta la inactividad, se cierra la sesión pasados unos minutos de inactividad
Al cerrar la sesión se borra el carrito

CARRITO(ADMIN,USER,VISITOR)
Si el carrito esta vació sale un mensaje de que debes añadir algún producto
Puedes cambiar la cantidad del producto desde el login
Sale los productos totales y la cantidad total a pagar
Al tramitar el pedido si no estas con ninguna sesión iniciada te enviá al login, si inicias sesión te realizara la compra automáticamente
Al pulsar el botón de tramitar si todo esta bien te guardara la compra en base de datos


TRANSLATE(ADMIN,USER,VISITOR)
Pagina en 3 idiomas(Español, Ingles, Valenciano)

Profile(ADMIN,USER)
Información del usuario con las compras que ha realizado.

DESIGN
Logotipo y Carrito personalizado con Inkscape con la extensión svg para que la imagen no pierda calidad y su tamaño este reducido









