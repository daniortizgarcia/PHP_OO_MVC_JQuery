<h3 data-tr="Perfil"></h3>
<div>
	<div style="width: 100%; display: inline-block; vertical-align: top; border: 2px solid; padding-bottom: 20px; padding-top: 20px; text-align: left;">
		
		<b>Usuario:</b> <span id="ContUser"></span><br>
		<b>Correo electronico:</b> <span id="ContMail"></span><br>
		<b>Nombre y Apellido:</b> <span id="ContName"></span>
		<span id="ContSurn"></span>
	</div>
	<h4 style="padding-bottom:20px; padding-top:20px;">Tus compras</h4>
	<div id='tablefromp' style="width: 80%; display: inline-block; padding-top: 20px;">
		<table id="purch" style="width:70%">
	     <thead>
	          <tr>
	            <th class='top_table_c' >Codigo Pedido</th>
	            <th class='top_table_c' >ImgProd</th>
	            <th class='top_table_c'>Product</th>
	            <th class='top_table_c' >Qty</th>
	            <th class='top_table_c' >Price</th>
	         </tr>
	     </thead>
	     <tbody id="cartBoda"></tbody>
		</table>
	</div>
	<div id='nopurch' class="alert alert-danger" style="margin-top: 30px;"></div>
	
</div>

<h4 style="padding-bottom:20px; padding-top:20px; ">Tus Favoritos</h4>
<div id='tablefav'>
	<table id="fav" style="width:70%">
	     <thead>
	          <tr>
	            <th class='top_table_c' >Codigo Produdcto</th>
	            <th class='top_table_c' >Imagen</th>
	            <th class='top_table_c'>Marca y modelo</th>
	            <th class='top_table_c' >Precio</th>
	         </tr>
	     </thead>
	     <tbody id="contFav"></tbody>
	</table>
</div>
<div id='nofav' class="alert alert-danger" style="margin-top: 30px;"></div>

