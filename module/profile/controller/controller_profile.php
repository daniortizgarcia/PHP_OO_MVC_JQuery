<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/miejer/PHP_OO_MVC_JQuery/';
	include ($path . 'module/profile/model/DAO_profile.php');
	if (isset($_SESSION["tiempo"])) {  
	    $_SESSION["tiempo"] = time();
	}
	@session_start();
	
	switch ($_GET['op']) {

		case 'list_profile':

			include("module/profile/view/list_profile.php");	
			break;
		case 'inpurch':
			if (isset($_SESSION['user'])) {
				try {
				$daoprof = new DAOprofile();
				$rlt = $daoprof->select_purchases($_SESSION['user']);
				} catch (Exception $e) {
					echo json_encode("error");
				}
				if (!$rlt) {
					echo json_encode("error");
				}else{
					$purch = array();
					foreach ($rlt as $value) {
						array_push($purch, $value);
					}
					if (!$purch) {
						echo "empty";
					}else{
						echo json_encode($purch);	
					}
					exit();
				}
			}else{
			}
			break;
		case 'inuser':
			if (isset($_SESSION['user'])) {
				try {
				$daoprof = new DAOprofile();
				$rlt = $daoprof->select_user($_SESSION['user']);
				} catch (Exception $e) {
					echo json_encode("error");
				}
				if (!$rlt) {
					echo json_encode("error");
				}else{
					$purch = array();
					foreach ($rlt as $value) {
						array_push($purch, $value);
					}
					echo json_encode($purch);	
					exit();
				}
			}else{
			}
			break;

		case 'addfav':
			try {
			$daoprof = new DAOprofile();
			$rlt = $daoprof->insert_fav($_SESSION['user'],$_GET['prod']);
			} catch (Exception $e) {
				echo json_encode("error");
			}
			if (!$rlt) {
				echo json_encode("error");
			}else{
				echo "ok";
			}
			break;
		case 'list_fav':
			try {
			$daoprof = new DAOprofile();
			$rlt = $daoprof->select_fav($_SESSION['user']);
			} catch (Exception $e) {
				echo json_encode("error");
			}
			if (!$rlt) {
				echo json_encode("error");
			}else{
				$fav = array();
				foreach ($rlt as $value) {
					array_push($fav, $value);
				}
				if ($fav) {
					echo json_encode($fav);	
				}else{
				}
				exit();
			}
			break;
		case 'delfav':
		print_r($_GET['fprod']);
			try {
				$daoprof = new DAOprofile();
				$rlt = $daoprof->delete_fav($_SESSION['user'],$_GET['fprod']);
			} catch (Exception $e) {
				echo json_encode("error");
			}
			if (!$rlt) {
				echo json_encode("error");
			}else{
				echo "ok";
			}
			break;

		default:
			include("view/inc/error404.php");
			break;
	}