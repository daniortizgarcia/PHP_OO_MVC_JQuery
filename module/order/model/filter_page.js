$(document).ready(function () {

    var url = "module/order/controller/controller_order.php?op=datatable";  
    // prepare the data
    var source =
    {
        dataType: "json",
        dataFields: [
            { name: 'cref', type: 'string' },
            { name: 'brand', type: 'string' },
            { name: 'model', type: 'string' },
            { name: 'price', type: 'string' }
        ],
        id: 'id',
        url: url
    };
    
    var dataAdapter = new $.jqx.dataAdapter(source);

    $("#dataTable").jqxDataTable(
    {
        width: getWidth("dataTable"),
        pageable: true,
        pagerButtonsCount: 10,
        source: dataAdapter,
        sortable: true,
        pageable: true,
        altRows: true,
        filterable: true,
        columnsResize: true,
        pagerMode: 'advanced',
        columns: [
          { text: 'Reference Code', dataField: 'cref'},
          { text: 'Brand', dataField: 'brand' },
          { text: 'Model', dataField: 'model'},
          { text: 'Price', dataField: 'price' }
      ]
    });  

});