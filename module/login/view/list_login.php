<div id="flogreg">

	<div id="flogin">
		<h3 style="text-align: center;">Login</h3>
		<form method="post" name="formlogin" id="formlogin" >

			<div class="bodyform">
				<label>Usuario:</label><br><input type="text" name="user" class="user">
				<br><span id="e_user" class="styerror"></span>
			</div>
			<br>
			<div class="bodyform">
				<label>contraseña:</label><br><input type="password" name="password" class="password">
				<br><span id="e_password" class="styerror"></span>
			</div><br>
			<div class="formbutton">
				<input id="buttonlogin" name="Submit" type="submit" value="Login" onclick="valide_login()" />
			</div>
			
		</form>
		<p id="error_login"></p>
		<a href="index.php?page=controller_login&op=list_recpass">¿Has olvidado la contraseña?</a>
	</div>

	<div id="fregister">
		<h3 style="text-align: center;">Register</h3>
		<form method="post" name="formregister" id="formregister" >

			<div class="bodyform">
				<label>Nombre:</label><br><input type="text" name="name" class="name" value="<?php echo $_POST?$_POST['name']:""; ?>">
				<br><span id="e_name" class="styerror"></span>
			</div>
			<br>
			<div class="bodyform">
				<label>Apellidos:</label><br><input type="text" name="surname" class="surmane" value="<?php echo $_POST?$_POST['surname']:""; ?>">
				<br><span id="e_surname" class="styerror"></span>
			</div>
			<br>

			<div class="bodyform">
				<label>Usuario:</label><br><input type="text" name="user" class="user" value="<?php echo $_POST?$_POST['user']:""; ?>">
				<br><span id="e_userr" class="styerror"></span>
			</div>
			<br>

			<div class="bodyform">
				<label>Correo electronico:</label><br><input type="text" name="mail" class="mail" value="<?php echo $_POST?$_POST['mail']:""; ?>">
				<br><span id="e_mail" class="styerror"></span>
			</div>
			<br>

			<div class="bodyform">
				<label>Contraseña:</label><br><input type="password" name="password" class="password" value="<?php echo $_POST?$_POST['password']:""; ?>">
				<br><span id="e_passwordr" class="styerror"></span>
			</div>
			<br>
			<div class="bodyform">
				<label>Repetir Contraseña:</label><br><input type="password" name="rpassword" class="rpassword" value="<?php echo $_POST?$_POST['rpassword']:""; ?>">
				<br><span id="e_rpasswordr" class="styerror"></span>
			</div>
			<br>
			<div class="formbutton">
				<input id="buttonregister" name="Submit" type="submit" value="Register" onclick="valide_register()" />
			</div>
		</form>
		<p id="error_register"></p>
	</div>

</div>