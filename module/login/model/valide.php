<?php

	function validate_register(){
		$error = array();
		$filters = array(
			'name' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^[A-za-z]{2,10}$/')
			),
			'surname' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^[A-z a-z]{2,15}$/')
			),
			'user' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^.{2,15}$/')
			),
			'mail' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/')
			),
			'password' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^.{6,30}$/')
			)
		);

		$result = filter_input_array(INPUT_POST,$filters);

		if (nd_user($result['user']) || !$result['user'])
			$error ='El formato del usuario es invalido o ya esta creado';
		elseif (!$result['name']) 
			$error ='El nombre no puede tener numeros y tiene que ser menor a 10 caracteres';
		elseif (!$result['surname']) 
			$error ='El apellido no puede tener mas de 15 caracteres';
		elseif (nd_mail($result['mail']) || !$result['mail']) {
			$error ='El formato del mail es invalido o ya esta creado';}
		elseif (!$result['password']) 
			$error ='La contraseña es invalida';
		else
			return $return=array('result'=>true,'error'=>$error,'data'=>$result);
		return $return=array('result'=>false , 'error'=>$error,'data'=>$result);

	}

	function validate_recpass(){
		$error = array();
		$filters = array(
			'mail' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/')
			),
			'password' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^.{6,30}$/')
			)
		);

		$result = filter_input_array(INPUT_POST,$filters);

		if (!nd_mail($result['mail'])){
			$error['mail'] ='El mail no existe en la base de datos';
		}
		elseif (!$result['password']){ 
			$error['password'] ='La contraseña es invalida';
		}
		else
			return $return=array('result'=>true,'error'=>$error,'data'=>$result);
		return $return=array('result'=>false , 'error'=>$error,'data'=>$result);

	}

	function nd_user($user){
		$daologin = new DAOlogin();
		$valide = $daologin->select_user($user);
		return $valide;
	}
	function nd_mail($mail){
		$daologin = new DAOlogin();
		$valide = $daologin->select_mail($mail);
		return $valide;
	}


?>