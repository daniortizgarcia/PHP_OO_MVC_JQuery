function valide_login(){

	//User

	if(document.formlogin.user.value.length === 0){
		document.getElementById('e_user').innerHTML = "Tienes que escribir el usuario";
		document.formlogin.user.focus();
		return 0;
	}

	document.getElementById('e_user').innerHTML = "";

	//Password

	if(document.formlogin.password.value.length === 0){
		document.getElementById('e_password').innerHTML = "Tienes que escribir la contraseña";
		document.formlogin.password.focus();
		return 0;
	}

	document.getElementById('e_password').innerHTML = "";

	document.formlogin.click();//click
	document.formlogin.action="index.php?page=controller_login&op=list_login";
}
function valide_register(){

	var mailp = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
	
	//Name
	if(document.formregister.name.value.length === 0){
		document.getElementById('e_name').innerHTML = "Tienes que escribir el nombre";
		document.formregister.name.focus();
		return 0;
	}

	if(document.formregister.name.value.length < 2){
		document.getElementById('e_name').innerHTML = "El nombre tiene que tener más de 2 caracteres";
		document.formregister.name.focus();
		return 0;
	}

	document.getElementById('e_name').innerHTML = "";

	//Surname

	if(document.formregister.surname.value.length === 0){
		document.getElementById('e_surname').innerHTML = "Tienes que escribir el apellido";
		document.formregister.surname.focus();
		return 0;
	}

	if(document.formregister.surname.value.length <= 2){
		document.getElementById('e_surname').innerHTML = "El apellido tiene que tener más de 2 caracteres";
		document.formregister.surname.focus();
		return 0;
	}

	document.getElementById('e_surname').innerHTML = "";

	//User

	if(document.formregister.user.value.length === 0){
		document.getElementById('e_userr').innerHTML = "Tienes que escribir el usuario";
		document.formregister.user.focus();
		return 0;
	}

	if(document.formregister.surname.value.length <= 2){
		document.getElementById('e_userr').innerHTML = "El usuario tiene que tener más de 2 caracteres";
		document.formregister.surname.focus();
		return 0;
	}

	document.getElementById('e_userr').innerHTML = "";

	//Mail

	if(document.formregister.mail.value.length === 0){
		document.getElementById('e_mail').innerHTML = "Tienes que escribir el mail";
		document.formregister.mail.focus();
		return 0;
	}
	if(!mailp.test(document.formregister.mail.value)){
		document.getElementById('e_mail').innerHTML = "El formato del mail es invalido";
		document.formregister.mail.focus();
		return 0;
	}

	document.getElementById('e_mail').innerHTML = "";

	//Password

	if(document.formregister.password.value.length === 0){
		document.getElementById('e_passwordr').innerHTML = "Tienes que escribir la contraseña";
		document.formregister.password.focus();
		return 0;
	}

	if(document.formregister.password.value.length < 6){
		document.getElementById('e_passwordr').innerHTML = "La contraseña tiene que tener más de 6 caracteres";
		document.formregister.password.focus();
		return 0;
	}

	document.getElementById('e_passwordr').innerHTML = "";

	//Password

	if(document.formregister.rpassword.value.length === 0){
		document.getElementById('e_rpasswordr').innerHTML = "Tienes que escribir la contraseña";
		document.formregister.rpassword.focus();
		return 0;
	}

	if(document.formregister.rpassword.value != document.formregister.password.value){
		document.getElementById('e_rpasswordr').innerHTML = "La contraseña tiene que ser la misma";
		document.formregister.rpassword.focus();
		return 0;
	}

	document.getElementById('e_rpasswordr').innerHTML = "";

	//document.formregister.click();
	//document.formregister.action="index.php?page=controller_login&op=list_register";
}

$(document).ready(function(){
	
	$("#formlogin").submit(function (e) {

		e.preventDefault();
		if(valide_login() != 0){
			var data = $("#formlogin").serialize();
			$.ajax({
				
			type : 'POST',
			url  : 'module/login/controller/controller_login.php?&op=login&' + data,
			data : data,
			beforeSend: function()
			{	
				$("#error_login").fadeOut();
			},
			success :  function(response)
			   {				
			   		console.log(response)		
					if(response=="ok"){
						setTimeout(' window.location.href = "index.php?page=controller_home&op=list"; ',1000);
					}else if (response=="okay") {
						InsertCompra();
					}else{
									
						$("#error_login").fadeIn(1000, function(){						
						$("#error_login").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
						});
					}
			  }
			});
		}
		
	});

	$("#formregister").submit(function (e) {

		e.preventDefault();
		if (valide_register() != 0) {
			var data = $("#formregister").serialize();
			$.ajax({
				
			type : 'POST',
			url  : 'module/login/controller/controller_login.php?&op=register&' + data,
			data : data,
			beforeSend: function()
			{	
				console.log(data)
				$("#error_register").fadeOut();
			},
			success :  function(response)
			   {						
					if(response==="ok"){
						setTimeout(' window.location.href = "index.php?page=controller_home&op=list"; ',1000);
					}
					else{
									
						$("#error_register").fadeIn(1000, function(){						
						$("#error_register").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+'</div>');
						});
					}
			  }
			});
		}
		
	});

});
 