function valide_recpass(){

	var mailp = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;

	//Mail

	if(document.formrecpass.mail.value.length === 0){
		document.getElementById('e_mail').innerHTML = "Tienes que escribir el mail";
		document.formrecpass.mail.focus();
		return 0;
	}
	if(!mailp.test(document.formrecpass.mail.value)){
		document.getElementById('e_mail').innerHTML = "El formato del mail es invalido";
		document.formrecpass.mail.focus();
		return 0;
	}

	document.getElementById('e_mail').innerHTML = "";

	//Password

	if(document.formrecpass.password.value.length === 0){
		document.getElementById('e_password').innerHTML = "Tienes que escribir la contraseña";
		document.formrecpass.password.focus();
		return 0;
	}

	if(document.formrecpass.password.value.length < 6){
		document.getElementById('e_password').innerHTML = "La contraseña tiene que tener más de 6 caracteres";
		document.formrecpass.password.focus();
		return 0;
	}

	document.getElementById('e_password').innerHTML = "";

	//Password

	if(document.formrecpass.rpassword.value.length === 0){
		document.getElementById('e_rpassword').innerHTML = "Tienes que escribir la contraseña";
		document.formrecpass.rpassword.focus();
		return 0;
	}

	if(document.formrecpass.rpassword.value != document.formrecpass.password.value){
		document.getElementById('e_rpassword').innerHTML = "La contraseña tiene que ser la misma";
		document.formrecpass.rpassword.focus();
		return 0;
	}

	document.getElementById('e_rpassword').innerHTML = "";

	document.formrecpass.click();
	document.formrecpass.action="index.php?page=controller_login&op=list_recpass";
}
$(document).ready(function(){

	$("#formrecpass").submit(function (e) {
		e.preventDefault();
		if (valide_recpass() != 0) {
			var data = $("#formrecpass").serialize();
			$.ajax({
				
			type : 'POST',
			url  : 'module/login/controller/controller_login.php?&op=update_pass&' + data,
			data : data,
			beforeSend: function()
			{	
				$("#error_recpass").fadeOut();
			},
			success :  function(response)
			   {						
					if(response=="ok"){
						setTimeout(' window.location.href = "index.php?page=controller_home&op=list"; ',1000);
					}
					else{			
						$("#error_recpass").fadeIn(1000, function(){						
						$("#error_recpass").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
						});
					}
			  }
			});
		}
		
	});

});
