<?php 
	$path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/miejer/PHP_OO_MVC_JQuery/';
    include($path . "module/login/model/DAO_login.php");
    include($path . "module/login/model/valide.php");
@session_start();

    switch ($_GET['op']) {

		case 'list_login':
			if (isset($_GET['purch']) && $_GET['purch'] === 'on'){
				$_SESSION['purchase'] = $_GET['purch'];
			}
				
			include("module/login/view/list_login.php");
			break;
		case 'register':
			
				$valide = validate_register();

				if($valide['result']){
					try {
					$daologin = new DAOlogin();
					$rlt = $daologin->insert_login($_POST['name'], $_POST['surname'], $_POST['user'], $_POST['mail'], $_POST['password']);
					
					} catch (Exception $e) {
						echo "Error al registrarse";
						exit();
					}
					if(!$rlt){
						echo "Error al registrarse";
						exit();
					}
					else{
						echo 'ok';
						exit();
					}	
				}
				else{
					echo $valide['error'];
					exit();
				}

			break;
		case 'login':

			try {
				$daologin = new DAOlogin();
				$rlt = $daologin->select_login($_POST['user']);
			
			} catch (Exception $e) {
				echo "error";
				exit();
			}
			if(!$rlt){
				echo "El usuario no existe";
				exit();
			}
			else{
				$value = get_object_vars($rlt);
				if (password_verify($_POST['password'],$value['password'])) {
					if (isset($_SESSION['purchase']) && $_SESSION['purchase'] === 'on')
						echo 'okay';
					else
						echo 'ok';
					$_SESSION['type'] = $value['type'];
					$_SESSION['user'] = $value['user'];
					$_SESSION['tiempo'] = time();
					exit();
				}else {
					echo "No coinciden los datos";
					exit();
				}
					
			}	
			break;

		case 'logout':
			session_unset($_SESSION['type']);

			if(session_destroy()) {
				$callback = 'index.php?page=controller_home&op=list';
			    die('<script>window.location.href="'.$callback .'";</script>');
			}
			break;

		case 'list_recpass':
			include("module/login/view/list_recpass.php");
			break;

		case 'update_pass':


			$valide = validate_recpass();

			if($valide['result']){
				try {
					$daologin = new DAOlogin();
					$rlt = $daologin->recovery_password($_POST['mail'],$_POST['password']);
				} catch (Exception $e) {
					echo "No coinciden los datos";
					exit();
				}
				if(!$rlt){
					echo "No coinciden los datos";
					exit();
				}
				else{
					echo "ok";
					exit();
				}		
			}else {
				foreach ($valide['error'] as $value) {
					echo $value;
				}
				exit();
			}
			
			break;
		case 'actividad':
			if (!isset($_SESSION["tiempo"])) {  
    	  		echo "activo";
			} else {  
	    		if((time() - $_SESSION["tiempo"]) >= 900) {  
	    	  		echo "inactivo"; 
	    	  		exit();
				}
				else{
					echo "activo";
					exit();
				}
			}
			
			break;
	
		default:
			include("view/inc/error404.php");
			break;
	}


?>