$(document).ready(function(){

  cart = [];
  $(function () {
      if (localStorage.cart){
        $("#confpurch").hide();
          cart = JSON.parse(localStorage.cart);
          showCart();
          $("#numcarr").empty();
          $("#numcarr").append("(" + cart.length + ")");
          if (localStorage.cart === '[]') {
            $( "#butttram" ).hide();
            $("#emptypurch").append("El carrito esta vacio");
          }else{
            $( "#butttram" ).show();
            $("#emptypurch").hide();
          }
      }else{
        $("#confpurch").hide();
        $( "#butttram" ).hide();
        $("#emptypurch").append("El carrito esta vacio");
      }
  });


  $(document).on('change','.changeqty', function() {  
        var changeqty = JSON.parse(localStorage.cart); 
        var nqty = $(this).val();

        for (var i in cart) {
            if(cart[i].cref ===  $(this).attr("id").substring(3,  $(this).attr("id").lenght)){
                cart[i].qty = nqty;
                showCart();
                saveCart();
                return;
            }
        }
    });


  $(document).on('click','.baddcart', function() {                  
    var id = $(this).attr("id");

    $.post("module/cart/controller/controller_cart.php?&op=product_shop&idprod=" + id, function(data,status) {
        
        var json = JSON.parse(data)
        var qty = $("#" + id + "qty").val();
        

        for (var i in cart) {
            if(cart[i].cref == json[0].cref){
                cart[i].qty = qty;
                showCart();
                saveCart();
                return;
            }
        }
        var prod = {cref: json[0].cref, qty: qty };

        cart.push(prod);
        saveCart();
        showCart();
        $("#numcarr").empty();
        $("#numcarr").append("(" + cart.length + ")");
    });

  });

});

function CPreu(data,qty,cont) {
  const promise = new Promise(function (resolve, reject) {


    $.get("module/cart/controller/controller_cart.php?&op=prod_price&idprod=" + data , function(data1,status) {
        json = JSON.parse(data1);
        json.qty=qty;
        json.cont=cont;

        resolve(json);
    });
    
    if (!data) {
      reject(new Error('No hay datos'));
    }
  });
  
  return promise;
}

function InsertCarr(){
  var id = $(".addmodal").attr("id");

  $.post("module/cart/controller/controller_cart.php?&op=product_shop&idprod=" + id, function(data,status) {
        
    var json = JSON.parse(data)
    var qty = $("#qty" + id + "modal").val();

    for (var i in cart) {
        if(cart[i].cref == json[0].cref){
            cart[i].qty = qty;
            showCart();
            saveCart();
            return;
        }
    }
    var prod = {cref: json[0].cref, qty: qty };

    cart.push(prod);
    saveCart();
    showCart();
    $("#numcarr").empty();
    $("#numcarr").append("(" + cart.length + ")");
  });
}

function DelCart(){
  localStorage.removeItem('cart');
}

function FinalData (data, array) {
  const promise = new Promise(function (resolve, reject) {
    setTimeout(function() {
      array.push(data)
      resolve(array)
    }, 100);
    
    if (!array) {
      reject(new Error('No existe un array'))
    }
  })
  
  return promise
}
const finaldata = [];


function InsertCompra() {
  if (!localStorage.cart) {
  }else{
    var prodata = [];
    var finaldata = [];
    $.get("module/cart/controller/controller_cart.php?&op=user_info", function(data,status) {
      if (data === 'error') {
        setTimeout(' window.location.href = "index.php?page=controller_login&op=list_login&purch=on"; ',1000);
      }else{
        var prod = JSON.parse(localStorage.cart);
        for (var i = prod.length - 1; i >= 0; i--) {
          CPreu(prod[i].cref, prod[i].qty, i).then(function (response) {
            var pdata = JSON.parse(JSON.stringify(response));
            prodata = [pdata.cref,data,pdata.qty,parseFloat(pdata.price) * parseFloat(pdata.qty)];

            FinalData(prodata,finaldata).then(function () {
              console.log(finaldata);
            });
          });
        }
        setTimeout(function() {
          console.log(finaldata)
          $.post("module/cart/controller/controller_cart.php?&op=add_info&dinfo=" + finaldata, function(data,status) {
            console.log(data)
            DelCart();
            $( "#butttram" ).hide();
            $( "#cart" ).hide();
            $("#numcarr").empty();
            $("#sumatotal").empty();
            $("#confpurch").show();
            $("#confpurch").append("Compra realizada correctamente");
            
            setTimeout('window.location.replace("index.php?page=controller_home&op=list");',2000);
          });  

        }, 1000);
          
      }

    });
  }

}


function saveCart() {
    if ( window.localStorage){
        localStorage.cart = JSON.stringify(cart);
    }
}

function showCart() {
    if (cart.length == 0) {
        $("#cart").css("visibility", "hidden");
      return;
    }

    $("#cart").css("visibility", "visible");
    $("#cartBody").empty();
    var sumatot = 0;
    var suma = 0;
    var sumprod = 0;
    var num = 0
    for (var i in cart) {
        var item = cart[i];

        CPreu(item.cref, item.qty, i).then(function (response) {
          var pdata = JSON.parse(JSON.stringify(response));

          suma = parseFloat(pdata.price) * parseFloat(pdata.qty);
          sumatot = parseFloat(sumatot) + parseFloat(suma);
          sumprod = parseInt(sumprod) + parseInt(pdata.qty);

          var row = "<tr><td style='width:150px;'><img src='" + pdata.img + "' style='width:100%;'></td><td>" + pdata.brand + " " + pdata.model + "</td><td>" + pdata.size + "</td><td>" +
                       pdata.price + "</td><td><select class='changeqty' id='qty" + pdata.cref +"'></select></td><td>"
                       + suma + "€</td><td>"
                       + "<button onclick='deleteItem(" + pdata.cont + ")'>Delete</button></td></tr>";
          $("#cartBody").append(row);

          for(j=1;j <= 20;j++){
            if (j === parseInt(pdata.qty)){
              $( "#qty" + pdata.cref).append("<option value='" + j +"' selected>" + j +"</option>");
            }else{
              $( "#qty" + pdata.cref).append("<option value='" + j +"'>" + j +"</option>");
            }
          }


          $("#sumatotal").empty();
          $("#sumatotal").append("Productos: " + sumprod + " // Total a pagar: " + sumatot + "€" );
          $("#sumaprod").empty();
          $("#sumaprod").append();
        });
    }
}

function deleteItem(index){
    cart.splice(index,1); //delete item at index
    showCart();
    saveCart();
    $("#numcarr").empty();
    $("#numcarr").append("(" + cart.length + ")");
    if (localStorage.cart === '[]') {
      $( "#butttram" ).hide();
      $( "#cart" ).hide();
      $("#numcarr").empty();
      $("#sumatotal").empty();
      $("#emptypurch").show();
      $("#emptypurch").append("El carrito esta vacio");
    }else{
      $( "#butttram" ).show();
      $("#emptypurch").hide();
    }
}