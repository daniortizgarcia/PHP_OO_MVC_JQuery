<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/miejer/PHP_OO_MVC_JQuery/';
	include ($path . 'module/cart/model/DAO_cart.php');
	@session_start();
	if (isset($_SESSION["tiempo"])) {  
	    $_SESSION["tiempo"] = time();
	}
	
	switch ($_GET['op']) {

		case 'list_cart':

			include("module/cart/view/list_cart.php");	
			break;

		case 'product_shop':

			try {
				$daocart = new DAOcart();
				$rlt = $daocart->select_product($_GET['idprod']);
			} catch (Exception $e) {
				echo json_encode("error");
			}
			if (!$rlt) {
				echo json_encode("error");
			}else{
				$prod = array();
				foreach ($rlt as $value) {
					array_push($prod, $value);
				}
				echo json_encode($prod);
				exit();
			}
			break;
		case 'user_info';
				if (isset($_SESSION['user'])) {
					$info_user = $_SESSION['user'];
					echo $info_user;
				}else
					echo "error";
			break;
		case 'add_info';
			$info = $_GET['dinfo'];
			$tok = explode(',',$info);  
			$array1 = array();
			$array2 = array();
			$cont = 0;
			foreach ($tok as $value) {
				array_push($array1, $value);
				$cont++;
				if ($cont === 4) {
					array_push($array2, $array1);
					$cont = 0;
					$array1 = array();
				}
			}
			foreach ($array2 as $value) {
				$daocart = new DAOcart();
				$rlt = $daocart->insert_purch($value['0'],$value['1'],$value['2'],$value['3']);
			}
			if (!$rlt) {
				echo 'error';
			}else{
				echo 'ok';
			}
			break;
		case 'prod_price':
			try {
				$daocart = new DAOcart();
				$rlt = $daocart->select_price($_GET['idprod']);
			} catch (Exception $e) {
				echo json_encode("error");
			}
			if (!$rlt) {
				echo json_encode("error");
			}else{
				echo json_encode($rlt);
				exit();
			}
			break;

		default:
			include("view/inc/error404.php");
			break;
	}