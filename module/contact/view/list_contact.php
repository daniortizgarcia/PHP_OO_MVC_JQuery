<h3 style="text-align: center; margin-bottom: 50px;" data-tr="Contactanos"></h3>
<div id="contact">
<div id="form" style="width: 49%; display: inline-block;">
<h3 style="text-align: center;" data-tr="Formulario de Contacto"></h3>
<form method="post" name="formcontact" id="formcontact" >
		
	<?php
	if (isset($valide['error'])) {
		$error = $valide['error'];
	}
	if(isset($error['name'])){
		print ("<br><span class='styerror'>" . "* ". $error['name'] . "</span><br/>");
	}
	?>
	<br>
	<div class="bodyform">
		<label data-tr="Nombre:"></label><br><input type="text" name="name" class="name" value="<?php echo $_POST?$_POST['name']:""; ?>">
		<br><span id="e_name" class="styerror"></span>
	</div>
	<br>
	<?php
	if(isset($error['surname'])){
		print ("<br><span class='styerror'>" . "* ". $error['surname'] . "</span><br/>");
	}
	?>
	<div class="bodyform">
		<label data-tr="Apellidos:"></label><br><input type="text" name="surname" class="surmane" value="<?php echo $_POST?$_POST['surname']:""; ?>">
		<br><span id="e_surname" class="styerror"></span>
	</div>
	<br>
		<?php
	if(isset($error['mail'])){
		print ("<br><span class='styerror'>" . "* ". $error['mail'] . "</span><br/>");
	}
	?>
	<div class="bodyform">
		<label data-tr="Correo electronico:"></label><br><input type="text" name="mail" class="mail" value="<?php echo $_POST?$_POST['mail']:""; ?>">
		<br><span id="e_mail" class="styerror"></span>
	</div>
	<br>
		<?php
	if(isset($error['phone'])){
		print ("<br><span class='styerror'>" . "* ". $error['phone'] . "</span><br/>");
	}
	?>
	<div class="bodyform">
		<label data-tr="Teléfono:"></label><br><input type="text" name="phone" class="phone" value="<?php echo $_POST?$_POST['phone']:""; ?>">
		<br><span id="e_phone" class="styerror"></span>
	</div>
	<br>
	<?php
	if(isset($error['message'])){
		print ("<br><span class='styerror'>" . "* ". $error['message'] . "</span><br/>");
	}
	?>
	<div class="bodyform">
		<label data-tr="Mensaje:"></label><br><textarea name="message" class="message"><?php echo $_POST?$_POST['message']:""; ?></textarea>
		<br><span id="e_message" class="styerror"></span>
	</div>
	<div class="formbutton">
		<input id="buttoncontact" name="Submit" type="button" value="Register" onclick="valide_contact()" />
	</div>
	
</form>
	
</div>
	
<div id="googlemaps" style="width: 50%; display: inline-block; vertical-align: top;">
	<h3 style="text-align: center;margin-bottom: 80px;" data-tr="Donde encontrarnos"></h3>
	<div id="map" style="height: 300px;"></div>
</div>
</div>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQXVT1dXiqxoOKQen_9K84rN3y8n9K9oI&callback=initMap">
</script>
