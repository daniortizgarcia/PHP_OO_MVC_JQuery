function valide_contact(){

	var mailp = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
	var phonep = /^[9|8|7|6]\d{8}$/;
	
	//Name
	if(document.formcontact.name.value.length === 0){
		document.getElementById('e_name').innerHTML = "Tienes que escribir el nombre";
		document.formcontact.name.focus();
		return 0;
	}

	if(document.formcontact.name.value.length < 2){
		document.getElementById('e_name').innerHTML = "El nombre tiene que tener más de 2 caracteres";
		document.formcontact.name.focus();
		return 0;
	}

	document.getElementById('e_name').innerHTML = "";

	//Surname

	if(document.formcontact.surname.value.length === 0){
		document.getElementById('e_surname').innerHTML = "Tienes que escribir el apellido";
		document.formcontact.surname.focus();
		return 0;
	}

	if(document.formcontact.surname.value.length <= 2){
		document.getElementById('e_surname').innerHTML = "El apellido tiene que tener más de 2 caracteres";
		document.formcontact.surname.focus();
		return 0;
	}

	document.getElementById('e_surname').innerHTML = "";

	//Mail

	if(document.formcontact.mail.value.length === 0){
		document.getElementById('e_mail').innerHTML = "Tienes que escribir el mail";
		document.formcontact.mail.focus();
		return 0;
	}
	if(!mailp.test(document.formcontact.mail.value)){
		document.getElementById('e_mail').innerHTML = "El formato del mail es invalido";
		document.formcontact.mail.focus();
		return 0;
	}

	document.getElementById('e_mail').innerHTML = "";

	//Phone

	if(document.formcontact.phone.value.length === 0){
		document.getElementById('e_phone').innerHTML = "Tienes que escribir el telefono movil";
		document.formcontact.phone.focus();
		return 0;
	}

	if(!phonep.test(document.formcontact.phone.value)){
		document.getElementById('e_phone').innerHTML = "El formato del telefono movil es invalido";
		document.formcontact.phone.focus();
		return 0;
	}

	document.getElementById('e_phone').innerHTML = "";

	//Message

	if(document.formcontact.message.value.length === 0){
		document.getElementById('e_message').innerHTML = "Tienes que escribir el mensaje";
		document.formcontact.message.focus();
		return 0;
	}

	if(document.formcontact.message.value.length < 15){
		document.getElementById('e_message').innerHTML = "El mensaje tiene que tener minimo 15 caracteres";
		document.formcontact.message.focus();
		return 0;
	}

	document.getElementById('e_message').innerHTML = "";

	document.formcontact.submit();
	document.formcontact.action="index.php?page=controller_contact&op=list";
}



function initMap() {
var uluru = {lat: 38.8401558, lng: -0.6414721};
var map = new google.maps.Map(document.getElementById('map'), {
  zoom: 4,
  center: uluru
});

var contentString = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h1 id="firstHeading" class="firstHeading">Ontinyent</h1>'+
    '<div id="bodyContent">'+
    '<p><b>Ontinyent</b>es un municipio de la Comunidad Valenciana (España), capital de la comarca del Valle de Albaida, situada al sur de la provincia de Valencia y en el centro de las Comarcas Centrales. Cuenta con 35.534 habitantes.</p>'+
    '<p>Attribution: Ontinyent, <a href="https://es.wikipedia.org/wiki/Onteniente">'+
    '(last visited January 24, 2018).</p>'+
    '</div>'+
    '</div>';

var infowindow = new google.maps.InfoWindow({
  content: contentString
});

var marker = new google.maps.Marker({
  position: uluru,
  map: map,
  title: 'Uluru (Ayers Rock)'
});
marker.addListener('click', function() {
  infowindow.open(map, marker);
});
}

 