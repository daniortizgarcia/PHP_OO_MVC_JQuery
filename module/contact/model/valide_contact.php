<?php

	function validate_contact(){
		$error = array();
		$filters = array(
			'name' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^[A-za-z]{2,10}$/')
			),
			'surname' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^[A-z a-z]{2,15}$/')
			),
			'mail' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/')
			),
			'phone' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^[9|8|7|6]\d{8}$/')
			),
			'message' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^.{15,100}$/')
			)
		);

		$result = filter_input_array(INPUT_POST,$filters);

		if (!$result['name'])
			$error['name'] ='El nombre no puede tener numeros y tiene que ser menor a 10 caracteres';
		elseif (!$result['surname']) 
			$error['surname'] ='Los apellidos no puede tener numeros y tiene que ser menor a 15 caracteres';
		elseif (!$result['mail']) 
			$error['mail'] ='El formato del mail es invalido';
		elseif (!$result['phone'])
			$error['phone'] ='El formato del telefono es invalido';
		elseif (!$result['message'])
			$error['message'] ='El mensaje no puede tener mas de 100 caracteres';
		else
			return $return=array('result'=>true,'error'=>$error,'data'=>$result);
		return $return=array('result'=>false , 'error'=>$error,'data'=>$result);

	}