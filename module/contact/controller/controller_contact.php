<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/miejer/PHP_OO_MVC_JQuery/';
	include ($path . 'module/contact/model/valide_contact.php');
	include ($path . 'module/contact/model/DAOcontact.php');
	if (isset($_SESSION["tiempo"])) {  
	    $_SESSION["tiempo"] = time();
	}
	
	switch ($_GET['op']) {

		case 'list':
			
			if ($_POST) {
				$valide = validate_contact();
				
				if ($valide['result']) {
					try {
						$daocontact = new DAOcontact();
						$rlt = $daocontact -> insert_contact($_POST['name'], $_POST['surname'], $_POST['mail'], $_POST['phone'], $_POST['message']);
					} catch (Exception $e) {
						$callback = 'index.php?page=503';
			    		die('<script>window.location.href="'.$callback .'";</script>');
					}
					if(!$rlt){
						$callback = 'index.php?page=503';
					    die('<script>window.location.href="'.$callback .'";</script>');
					}
					else{
						echo '<script language="javascript">alert("Registrado en la base de datos correctamente")</script>';
            			$callback = 'index.php?page=controller_contact&op=list';
        			    die('<script>window.location.href="'.$callback .'";</script>');
					}
				}
			}

			include("module/contact/view/list_contact.php");	
			break;

		default:
			include("view/inc/error404.php");
			break;
	}