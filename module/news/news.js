$(document).ready(function(){

  if (document.getElementById('content_news')) {
    $.ajax({
      url: "https://newsapi.org/v2/top-headlines?sources=marca&apiKey=4825bdc215cc4c0cb72811d654f50da9",
      success(json) {
        var listElements = "";
          $.each(json.articles, function(index, list) {
            var ElementDiv = document.createElement('div');
            ElementDiv.id = "list_news";
            ElementDiv.innerHTML =  "<div class='header_news'><a href='" + list.url + "'><div class='img_news'><img src='" + list.urlToImage + "'></div></a></div>" +
                                    "<div class='content_news'><h6> " + list.title + "</h6><div class='description_news'><p> " + list.description +"</p></div></div>" +
                                    "<div class='footer_news'><p>" + list.publishedAt + "</p></div>";
            document.getElementById("shownm").appendChild(ElementDiv);
          });
      }
    })

    $.ajax({
      url: "https://newsapi.org/v2/top-headlines?sources=lequipe&apiKey=4825bdc215cc4c0cb72811d654f50da9",
      success(json) {
        var listElements = "";
          $.each(json.articles, function(index, list) {
            var ElementDiv = document.createElement('div');
            ElementDiv.id = "list_news";
            ElementDiv.innerHTML =  "<div class='header_news'><a href='" + list.url + "'><div class='img_news'><img src='" + list.urlToImage + "'></div></a></div>" +
                                    "<div class='content_news'><h6> " + list.title + "</h6><div class='description_news'><p> " + list.description +"</p></div></div>" +
                                    "<div class='footer_news'><p>" + list.publishedAt + "</p></div>";
            document.getElementById("showne").appendChild(ElementDiv);
          });
      }
    })
  }
  
});