<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/miejer/PHP_OO_MVC_JQuery/';
	include ($path . 'module/shop/model/DAO_shop.php');
	if (isset($_SESSION["tiempo"])) {  
	    $_SESSION["tiempo"] = time();
	}
	@session_start();
	
	switch ($_GET['op']) {

		case 'list_shop':

			include("module/shop/view/list_shop.php");	
			break;
		case 'data_shop':

			try {
				$daoshop = new DAOshop();
				$rlt = $daoshop->select_all_products();
			} catch (Exception $e) {
				echo json_encode("error");
			}
			if (!$rlt) {
				echo json_encode("error");
			}else{
				$prod = array();
				foreach ($rlt as $value) {
					array_push($prod, $value);
				}
				echo json_encode($prod);
				exit();
			}
			break;
		case 'list_modal':
			try {
				$daoshop = new DAOshop();
				$rlt = $daoshop->select_product($_GET['modal']);
			} catch (Exception $e) {
				echo json_encode("error");
			}
			if (!$rlt) {
				echo json_encode("error");
			}else{
				echo json_encode($rlt);
				exit();
			}	
			break;


		default:
			include("view/inc/error404.php");
			break;
	}