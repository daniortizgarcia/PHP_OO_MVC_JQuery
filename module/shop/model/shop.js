$(document).ready(function(){

  if (document.getElementById('container_shop')) {
  	$.get("module/shop/controller/controller_shop.php?&op=data_shop", function(data,status) {
        var json = JSON.parse(data);
     		$.each(json, function(index, list) {

          	var ElementDiv = document.createElement('div');
          	ElementDiv.id = "list_shop";
          	ElementDiv.innerHTML = 	"<div id='cont_img'><img src='" + list.img + "' class='modaldet' id='" + list.cref + "' data-toggle='modal' data-target='#exampleModal'></div><div id='list_header'><span id='li_brand'> Zapatilla " + list.brand + " " + list.model + "</span></div>" +
          						   	"<div id='list_footer'><span id='" + list.cref + "pr'>" + list.price + "</span><div id='cont_comprar'><select class='qtyprod' id='" + list.cref + "qty'></select><button class='baddcart' id='" + list.cref + "'>Add to cart</button></div><button class='addfav' id='" + list.cref + "'>Favorite <3</button></div>";
          	document.getElementById("list_products").appendChild(ElementDiv);
        });

        for(i=1;i<=20;i++){
          $( ".qtyprod" ).append("<option value='" + i +"'>" + i +"</option>");
        }

  	});
  }


  $(document).on('click','.modaldet',function () {
      $("#modal_products").empty();
      $("#exampleModalLabel").empty();
        var id = this.getAttribute('id');
        
    $.get("module/shop/controller/controller_shop.php?op=list_modal&modal=" + id, function (data, status) {
          var json = JSON.parse(data);
           $("#exampleModalLabel").append("Zapatillas " + json.brand + " " + json.model);
          var ElementDiv = document.createElement('div');
          ElementDiv.id = "modal_shop";
          ElementDiv.innerHTML =  "<div style='width:46%; display:inline-block;' class='provamodal'><img src='" + json.img + "' style='width:100%;'></div>" +
                                  "<div style='width:44%; display:inline-block; vertical-align:top; font-size:30px; margin-left:10%;'><div style='width:100%;'> Price: " + json.price + "€</div>" +
                                  "<div style='width:100%;'> Size: " + json.size + "</div><div style='font-size: 18px;text-align:  justify;margin-top: 44px;margin-right: 33px'>" + json.description + "</div>" +
                                  "<select id='qty" + json.cref + "modal' style='width: 60%; font-size:20px; margin-top: 53px; border: solid 3px #bbbbbb; background-color: #808080bd; color: white;'></select></div>";
          document.getElementById("modal_products").appendChild(ElementDiv);

          for(i=1;i<=20;i++){
            $( "#qty" + json.cref + "modal" ).append("<option value='" + i +"'>" + i +"</option>");
          }

          $(".addmodal").attr("id",json.cref);
    });
  });
});