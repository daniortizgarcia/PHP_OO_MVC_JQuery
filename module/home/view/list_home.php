<div id="contenido">
    <div class="container" id="container_home">

        <h1 data-tr="Ultimas Novedades"></h1>

        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
              <img class="d-block img-fluid" src="view/img/carrumunich.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
              <img class="d-block img-fluid" src="view/img/carruadidas.jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
              <img class="d-block img-fluid" src="view/img/carrunike.jpg" alt="Third slide">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

    			
    	<div class="brand_container">
            <h1 data-tr="Marcas que puedes encontrar"></h1>
            <?php
                if ($rlt->num_rows === 0){
                    echo '<td align="center" colspan="3">No se ha creado ninguna zapatilla</td>';
                }else{


                echo '<div id="home_flat">';
                echo '<h3 data-tr="Fútbol Sala"></h3>';
                    foreach ($rlt as $row) {
                        if (substr($row['cbrand'], -1) === "a") {
                            echo '<div class="shoes_home">';
                                echo '<div class="dum_img">';
                                    echo '<img src="'. $row['img'] .'">';
                                echo '</div>';
                                echo '<div class="dum_bra">';
                                    echo '<span>' . $row['brand'] . '</span>';
                                echo '</div>';
                            echo '</div>';
                        }
                    }
                echo '</div>';
                echo '<div class="home_cleats">';
                echo '<h3 data-tr="Fútbol"></h3>';
                    foreach ($rlt as $row) {
                        if (substr($row['cbrand'], -1) === "b") {
                            echo '<div class="shoes_home">';
                                echo '<div class="dum_img">';
                                    echo '<img src="'. $row['img'] .'">';
                                echo '</div>';
                                echo '<div class="dum_bra">';
                                    echo '<span>' . $row['brand'] . '</span>';
                                echo '</div>';
                            echo '</div>';
                        }
                    }
                echo '</div>';
                }
            ?>
    	</div>




    <div class="divContenedor">
        <h2 data-tr="Filtrar Zapatillas"></h2>
        <div class="divLabels">
            <label for="cboSole">Tipos de Suela</label>
        </div>
        <div class="divSelects">
            <select id="cboSole">
                <option value="0">Seleccione una Suela</option>
            </select>
        </div>

        <br><br>
        <div class="divLabels">
            <label for="cboSize">Tallas</label>
        </div>
        <div class="divSelects">
            <select id="cboSize">
                <option value="0">Seleccione una Suela</option>
            </select>
        </div>
    </div>  
    
     <br><br>
     <input type="text" size="50" id="service" name="service" />
     <div id="suggestions"></div>
    
     

     <div id="filterlist">
         
         
     </div>

</div>