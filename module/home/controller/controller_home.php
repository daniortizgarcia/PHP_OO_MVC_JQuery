<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/miejer/PHP_OO_MVC_JQuery/';
	include($path . "module/home/model/DAO_home.php");
	@session_start();

	if(!$_GET['op']){
		$_GET['op']='list';
	} 
if (isset($_SESSION["tiempo"])) {  
    $_SESSION["tiempo"] = time();
}

	switch ($_GET['op']) {

		case 'list':
			try{
				$daohome = new DAOhome();
				$rlt = $daohome->select_all_dummies();
			} catch(Exception $e){
				$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
			}

			if(!$rlt){
				$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
			}
			else{
				include("module/home/view/list_home.php");
			}
			break;
		case 'drop_sole':

			try{
				$daohome = new DAOhome();
				$rlt = $daohome->select_tsole();
			} catch(Exception $e){
				echo json_encode("error");
			}

			if(!$rlt){
				echo json_encode("error");
			}
			else{
				$dinfo = array();
				foreach ($rlt as $row) {
					array_push($dinfo, $row);
				}
				echo json_encode($dinfo);
			}
			break;
		case 'drop_brands':

			try{
				$daohome = new DAOhome();
				$rlt = $daohome->select_size($_GET['sole']);
			} catch(Exception $e){
				echo json_encode("error");
			}

			if(!$rlt){
				echo json_encode("error");
			}
			else{
				$dinfo = array();
				foreach ($rlt as $row) {
					array_push($dinfo, $row);
				}
				echo json_encode($dinfo);
			}
			break;
		case 'autocomplet':

			try{
				$daohome = new DAOhome();
				$rlt = $daohome->select_all_shoes($_GET['sole'],$_GET['size']);
			} catch(Exception $e){
				echo json_encode("error");
			}

			if(!$rlt){
				echo json_encode("error");
			}
			else{
				foreach ($rlt as $row) {
					 echo '<div id="cont_auto">
					 	   <a class="suggest_element" data="'.$row['brand'].'" id="service'.$row['cref'].'">'.utf8_encode($row['brand']).'</a>
                    	   </div>';
				}
				exit;
			}
			break;
		case 'filter_list':
			try{

				$daohome = new DAOhome();
				$rlt = $daohome->select_filter_list($_GET['sole'],$_GET['size'],$_GET['brand']);
			} catch(Exception $e){
				echo json_encode("error");
			}

			if(!$rlt){
				echo json_encode("error");
			}
			else{
				$dinfo = array();
				foreach ($rlt as $row) {
					array_push($dinfo, $row);
				}
				echo json_encode($dinfo);
			}			
			break;
		
		default:
			include("view/inc/error404.php");
			break;
	}