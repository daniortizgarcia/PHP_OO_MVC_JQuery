$(document).ready(function(){
	
	$.get("module/home/controller/controller_home.php?&op=drop_sole", function (data, status) {
		var json = JSON.parse(data);

		var $cboSole = $("#cboSole");
	    $cboSole.empty();
        $.each(json, function(index, sole) {
            $cboSole.append("<option>" + sole.t_sole + "</option>");
        });
 	});

    $("#cboSole").change(function() {

    	var sole = $(this).val();

        $.get("module/home/controller/controller_home.php?&op=drop_brands&sole=" + sole, function (data, status) {
			var json = JSON.parse(data);

			var $cboSize = $("#cboSize");
		    $cboSize.empty();

		    $.each(json, function(index, sizes) {
		        $cboSize.append("<option>" + sizes.size + "</option>");
		    });
		});

        //Al escribr dentro del input con id="service"
	    $('#service').keyup(function(){
	        //Obtenemos el value del input
	        var services = $(this).val(); 
	        var vsize = document.getElementById("cboSize").value;

	        var dataString = {service: services};
	        //Le pasamos el valor del input al ajax
	        $.ajax({
	            type: "POST",
	            url: "module/home/controller/controller_home.php?&op=autocomplet&sole=" + sole +"&size=" + vsize,
	            data: dataString,
	            success: function(data) {
	                //Escribimos las sugerencias que nos manda la consulta
	                $('#suggestions').fadeIn(1000).html(data);
	                //Al hacer click en algua de las sugerencias
	                $('.suggest_element').on('click', function(){
	                    //Obtenemos la id unica de la sugerencia pulsada
	                    var id = $(this).attr('id');
	                    //Editamos el valor del input con data de la sugerencia pulsada
	                    $('#service').val($('#'+id).attr('data'));
	                    //Hacemos desaparecer el resto de sugerencias
	                    $('#suggestions').fadeOut(1000);

	                    var vbrand = document.getElementById("service").value;

		                 $.get("module/home/controller/controller_home.php?&op=filter_list&sole=" + sole +"&size=" +  vsize +"&brand=" +  vbrand, function (data, status) {
							var json = JSON.parse(data);
							var $FList = $("#filterlist");


						    $FList.empty();

					        $.each(json, function(index, list) {

					        	var ElementDiv = document.createElement('div');
					        	ElementDiv.id = "list_filter";
					        	ElementDiv.innerHTML = 	"<div id='list_header'><span id='li_brand'> Brand: " + list.brand + "</span><span id='li_model'> Model:  " + list.model + "</span></div>" +
					        						   	"<div id='list_footer'><span id='li_size'>Price: " + list.price + "</span></div>";
					        	document.getElementById("filterlist").appendChild(ElementDiv);
					        });
					 	});  
	                });              
	            }
	        });
	    }); 
	});

});