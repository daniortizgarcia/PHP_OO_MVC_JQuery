<?php

	function validate_shoes($option){
		$error = array();
		$filters = array(
			'cref' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/[0-9]{5}[\-][a-y]{1}/')
			),
			'brand' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^[A-za-z]{2,10}$/')
			),
			'model' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^.{2,10}$/')
			),
			'mail' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/')
			),
			'datep' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/[0-9]{2}[\/][0-9]{2}[\/][0-9]{4}/')
			),
			'datep2' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/[0-9]{2}[\/][0-9]{2}[\/][0-9]{4}/')
			),
			'description' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^.{15,100}$/')
			),
			'price' => array(
				'filter' => FILTER_VALIDATE_REGEXP, 
				'options' => array('regexp' => '/^[0-9]{1,5}[,.0]{0,1}[0-9]{0,2}$/')
			)
		);

		$result = filter_input_array(INPUT_POST,$filters);

		if($option === "create"){
			if (nd_id($result['cref']) || !$result['cref'])
				$error['cref'] ='El formato del codigo de referencia es invalido o ya esta creado';
			elseif (!$result['brand']) 
				$error['brand'] ='La marca no puede tener numeros y tiene que ser menor a 10 caracteres';
			elseif (!$result['model']) 
				$error['model'] ='El modelo no puede tener mas de 10 caracteres';
			elseif (!$result['mail']) 
				$error['mail'] ='El formato del mail es invalido';
			elseif (!c_dates($result['datep'])) 
				$error['datep'] ='El comienzo de la promoción no puede ser anterior a la fehca actual'; 
			elseif (!$result['datep2'])
				$error['datep2'] ='El formato de la data es invalido';
			elseif (!$result['description'])
				$error['description'] ='La descripción no puede tener mas de 100 caracteres';
			elseif (!$result['price'])
				$error['price'] ='El formato del precio es incorrecto';
			else
				return $return=array('result'=>true,'error'=>$error,'data'=>$result);
			return $return=array('result'=>false , 'error'=>$error,'data'=>$result);
		}else{
			if (!$result['cref'])
				$error['cref'] ='El formato del codigo de referencia es invalido o ya esta creado';
			elseif (!$result['brand']) 
				$error['brand'] ='La marca no puede tener numeros y tiene que ser menor a 10 caracteres';
			elseif (!$result['model']) 
				$error['model'] ='El modelo no puede tener mas de 10 caracteres';
			elseif (!$result['mail']) 
				$error['mail'] ='El formato del mail es invalido';
			elseif (!c_dates($result['datep'])) 
				$error['datep'] ='El comienzo de la promoción no puede ser anterior a la fecha actual'; 
			elseif (!$result['datep2'])
				$error['datep2'] ='El formato de la data es invalido';
			elseif (!$result['description'])
				$error['description'] ='La descripción no puede tener mas de 100 caracteres';
			elseif (!$result['price'])
				$error['price'] ='El formato del precio es incorrecto';
			else
				return $return=array('result'=>true,'error'=>$error,'data'=>$result);
			return $return=array('result'=>false , 'error'=>$error,'data'=>$result);
		}
		

	}

	
	function c_dates($dateu){
		$thisdate = getdate();
		if(strtotime($dateu) < strtotime($thisdate['mon'] . "/" . $thisdate['mday'] . "/" . $thisdate['year'])){
			return false;
		} else{
			return true;
		}
	}
	function c_dateu($dateu, $dateu2){
		$thisdate = getdate();
		if(strtotime($dateu2) < strtotime($dateu))
			return false;
		else
			return true;
	} 
	function nd_id($cref){
		$daoshoes = new DAOshoes();
		$valide = $daoshoes->select_shoes($cref);
		return $valide;
	}


?>