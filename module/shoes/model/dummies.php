<?php

	function load_dummies(){
		$cbrand = brand();
		$daoshoes = new DAOshoes();
		$rlt = $daoshoes->insert_shoes(c_ref(), $cbrand, model($cbrand), fmail(), sole_t(), size(), "05/05/2018", "06/06/2018", description(), terrain(), price());

		return $rlt;
	}
	function c_ref() {

			$cad = "";
			$letras = array("q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","ñ","x","c","v","b","n","m","ç");
			
			for ($i=0; $i < 5; $i++) { 
				$cad .= rand(0, 9);
			}
			
			return $cad . "-" . $letras[rand(0, 26)];
	}
	function brand() {
		$t_brand = array("Adidas", "Nike", "Kelme", "Joma", "Munich", "Mizuno", "Penalty", "Kipsta" , "Puma" , "Umbro", "Lotto");
		return $t_brand[rand(0, 10)];
	}

	function model($brand) {
		$t_model = array("Nemeziz 17", "Ace 17", "Predator Precision", "Hypervenom", "Magista", "Mercurial", "Feline", "Subito", "Precision", 
							 "Propulsion", "Sala max", "Dribling", "Mundial", "Continental", "Gresca", "Monarcida", "Sala premium", "Sala clasic",
							 "Commander", "Matis", "Max 400", "CLR300", "Agility", "CLR 500", "Evopower", "EvoSpeed", "Evotouch",
							 "Eternal", "Medusa", "clasic", "Zhero gravity 200", "Zhero gravity 700", "Spider");
		$model = "";

			switch($brand) {
				case "Adidas":
					$model = $t_model[rand(0, 2)];//Nemeziz 17, Ace 17, Predator Precision.
					break;
				case "Nike":
					$model = $t_model[rand(0, 2) + 3];//Hypervenom, Magista, Mercurial
					break;
				case "Kelme":
					$model = $t_model[rand(0, 2) + 6];//Feline, Subito, Precision
					break;
				case "Joma":
					$model = $t_model[rand(0, 2) + 9];//Propulsion, Sala max, Dribling
					break;
				case "Munich":
					$model = $t_model[rand(0, 2) + 12];//Mundial, Continental, Gresca
					break;
				case "Mizuno":
					$model = $t_model[rand(0, 2) + 15];//Monarcida, Sala premium, Sala clasic
					break;
				case "Penalty":
					$model = $t_model[rand(0, 2) + 18];//Commander, Matis, Max 400
					break;
				case "Kipsta":
					$model = $t_model[rand(0, 2) + 21];//CLR300, Agility, CLR 500
					break;
				case "Puma":
					$model = $t_model[rand(0, 2) + 24];//Evopower, Evospeed, Evotouch
					break;
				case "Umbro":
					$model = $t_model[rand(0, 2) + 27];//Eternal, Medusa, Clasic
					break;
				case "Lotto":
					$model = $t_model[rand(0, 2) + 30];//Zhero gravity 200, Zhero gravity 700, Spider
					break;
			}
			return $model;
	}
	function fmail() {
		$t_email = array("dani@gmail.com", "pepe@gmail.com", "manolo@gmail.com", "esteban@gmail.com", "aleja@gmail.com", "adri@gmail.com");
		return $t_email[rand(0, 5)];
	}
	function size() {
		return rand(33, 46);
	}
	function price() {
		return (rand(33, 150) + ".00");
	}
	function sole_t() {
		$t_sole = array('Flat', 'Cleats', 'MiniCleats');
		return $t_sole[rand(0, 2)];
	}
	function description(){
		$t_description = array('Zapatillas muy cómodas con muy buen agarre','Son el modelo mas nuevo, con la mejor tecnología','La mejores zapatillas que puedes encontrar en todo el mercado','Mejores zapatillas precio calidad',"Las mejores zapatillas para un delantero de verdad",'Nunca estarás mas cómodo que con estas zapatillas');
		return $t_description[rand(0, 5)];
	}
	function terrain(){
		$t_terrain = array('Artificial Grass', 'Natural Grass', 'Cement', 'Parquet');
		$contador = rand(1,4);
		$cad = array();
		for ($i=0; $i < $contador; $i++) { 
			$content = $t_terrain[rand(0, 3)];
			if(!in_array($content, $cad)){
				array_push($cad, $content);
			}
		}
		return $cad;
	}
	?>