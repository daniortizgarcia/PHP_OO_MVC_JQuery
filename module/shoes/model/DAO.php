<?php 

	$path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/miejer/PHP_OO_MVC_JQuery/';
    include($path . "model/connect.php");
	class DAOshoes{
		
		function insert_shoes($cref, $brand, $model, $mail, $t_sole, $size, $datep, $datep2, $description, $terrain, $price){

			$cad = "";
			$count = 0;

			foreach ($terrain as $indice => $value) {
				$count++;
				if ($count != count($terrain))
					$cad .= "$value,";
				else
					$cad .= "$value";
			}

			$sql = "INSERT INTO shoes(cref, brand, model, mail, t_sole, size, datep, datep2, description, terrain, price, hora, fecha) 
					VALUES('$cref', '$brand', '$model', '$mail', '$t_sole', '$size', '$datep', '$datep2', '$description', '$cad', '$price', now(), now())";

			$connection = connect::con();

			$res = mysqli_query($connection, $sql);
			connect::close($connection);
			return $res;
		}


		function select_all_shoes(){
			$sql = "SELECT * FROM shoes ORDER BY brand,model,size ASC";
			$connection = connect::con();
			$res = mysqli_query($connection, $sql);
			connect::close($connection);
			return $res;
		}
		function select_all_dummies(){
			$sql = "SELECT * FROM dummies ORDER BY brand ASC";
			$connection = connect::con();
			$res = mysqli_query($connection, $sql);
			connect::close($connection);
			return $res;
		}
		function select_shoes($cref){
			$sql = "SELECT * FROM shoes WHERE cref='$cref'";
			$connection = connect::con();
			$res = mysqli_query($connection, $sql)->fetch_object();
			connect::close($connection);
			return $res;
		}
		function update_shoes($cref, $brand, $model, $mail, $t_sole, $size, $datep, $datep2, $description, $terrain, $price){

			$cad = "";
			$count = 0;
			foreach ($terrain as $indice => $value) {
				$count++;
				if ($count != count($terrain))
					$cad .= "$value,";
				else
					$cad .= "$value";
			}
			$sql = " UPDATE shoes SET brand='$brand', model='$model', mail='$mail', t_sole='$t_sole', size='$size', datep='$datep',"
        		. " datep2='$datep2', description='$description', terrain='$cad', price='$price' WHERE cref='$cref'";

        	$connection = connect::con();
			$res = mysqli_query($connection, $sql);
			connect::close($connection);
			return $res;
		}
		function delete_shoes($cref){
			$sql = "DELETE FROM shoes WHERE cref='$cref'";
			$connection = connect::con();
			$res = mysqli_query($connection, $sql);
			connect::close($connection);
			return $res;
		}
	}