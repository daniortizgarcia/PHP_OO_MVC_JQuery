function validate_shoes(redirect) {


	var crefp = /[0-9]{5}[-][a-z]{1}/;
	var mailp = /^^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
	var date_p = /[0-9]{2}[/][0-9]{2}[/][0-9]{4}/;
	var pricep = /^[0-9]{2}$/;
	var pricep2 = /^[0-9]{1,5}[,.]{0,1}[0-9]{0,2}$/;
	

	//Cref 
	if(document.formshoes.cref.value.length === 0){
		document.getElementById('e_cref').innerHTML = "Tienes que escribir el codigo de referencia";
		document.formshoes.cref.focus();
		return 0;
	}

	if(!crefp.test(document.formshoes.cref.value)){
		document.getElementById('e_cref').innerHTML = "El formato del codigo de referencia es invalido";
		document.formshoes.cref.focus();
		return 0;
	}

	document.getElementById('e_cref').innerHTML = "";

	//Brand

	if(document.formshoes.brand.value.length === 0){
		document.getElementById('e_brand').innerHTML = "Tienes que escribir la marca";
		document.formshoes.brand.focus();
		return 0;
	}

	if(document.formshoes.brand.value.length <= 2){
		document.getElementById('e_brand').innerHTML = "La marca tiene que tener más de 2 caracteres";
		document.formshoes.brand.focus();
		return 0;
	}

	document.getElementById('e_brand').innerHTML = "";

	//Model

		if(document.formshoes.model.value.length === 0){
		document.getElementById('e_model').innerHTML = "Tienes que escribir el modelo";
		document.formshoes.model.focus();
		return 0;
	}

	if(document.formshoes.model.value.length <= 2){
		document.getElementById('e_model').innerHTML = "El modelo tiene que tener más de 2 caracteres";
		document.formshoes.model.focus();
		return 0;
	}

	document.getElementById('e_model').innerHTML = "";

	//Mail

	if(document.formshoes.mail.value.length === 0){
		document.getElementById('e_mail').innerHTML = "Tienes que escribir el mail";
		document.formshoes.mail.focus();
		return 0;
	}
	if(!mailp.test(document.formshoes.mail.value)){
		document.getElementById('e_mail').innerHTML = "El formato del mail es invalido";
		document.formshoes.mail.focus();
		return 0;
	}

	document.getElementById('e_mail').innerHTML = "";

	//Start Promotion

	if(document.formshoes.datep.value.length === 0){
		document.getElementById('e_datep').innerHTML = "Tienes que elegir la fecha";
		document.formshoes.datep.focus();
		return 0;
	}

	if(!date_p.test(document.formshoes.datep.value)){
		document.getElementById('e_datep').innerHTML = "El formato de la fecha es invalido";
		document.formshoes.datep.focus();
		return 0;
	}

	document.getElementById('e_datep').innerHTML = "";

	//End Promotion

	if(document.formshoes.datep2.value.length === 0){
		document.getElementById('e_datep2').innerHTML = "Tienes que elegir la fecha";
		document.formshoes.datep2.focus();
		return 0;
	}

	if(!date_p.test(document.formshoes.datep2.value)){
		document.getElementById('e_datep2').innerHTML = "El formato de la fecha es invalido";
		document.formshoes.datep2.focus();
		return 0;
	}
	document.getElementById('e_datep2').innerHTML = "";

	//Description

	if(document.formshoes.description.value.length === 0){
		document.getElementById('e_description').innerHTML = "Tienes que escribir la descripción";
		document.formshoes.description.focus();
		return 0;
	}

	if(document.formshoes.description.value.length < 15){
		document.getElementById('e_description').innerHTML = "La descripción tiene que tener minimo 15 caracteres";
		document.formshoes.description.focus();
		return 0;
	}

	document.getElementById('e_description').innerHTML = "";

	//Price

	if(document.formshoes.price.value.length === 0){
		document.getElementById('e_price').innerHTML = "Tienes que escribir el precio";
		document.formshoes.price.focus();
		return 0;
	}
	if(!pricep2.test(document.formshoes.price.value)){
		document.getElementById('e_price').innerHTML = "El formato del precio es incorrecto";
		document.formshoes.price.focus();
		return 0;
	}
	

	document.getElementById('e_price').innerHTML = "";

	//Terrain

	if(!v_terrain(document.getElementsByName("terrain[]"))){
		document.getElementById('e_terrain').innerHTML = "Tienes que elegir una opcion";
		//document.getElementsByName("terrain").focus();
		return 0;
	}

	document.getElementById('e_terrain').innerHTML = "";

	document.formshoes.submit();
	if (redirect === 'create') {
		document.formshoes.action="index.php?page=controller_shoes.php&op=create";
	}else{
		document.formshoes.action="index.php?page=controller_shoes.php&op=update";
	}
	
}

function v_terrain(valide){
	var confirm = false;
		for(i=0; i<valide.length;i++){
	        if(valide[i].checked){
	            return true;
	            confirm = true;
	        }
   		}
   		if (!confirm) {
	       	return false;
	    }
	}



$(document).ready(function () {
    $('.read_modal').click(function () {
        var id = this.getAttribute('id');
		$.get("module/shoes/controller/controller_shoes.php?op=read_modal&modal=" + id, function (data, status) {
            var json = JSON.parse(data);
            
            if(json === 'error') {
                //console.log(json);
                //pintar 503
			    window.location.href='index.php?page=503';
            }else{

                $("#cref").html(json.cref);
                $("#brand").html(json.brand);
                $("#model").html(json.model);
                $("#mail").html(json.mail);
                $("#t_sole").html(json.t_sole);
                $("#size").html(json.size);
                $("#datep").html(json.datep);
                $("#datep2").html(json.datep2);
                $("#terrain").html(json.terrain);
                $("#description").html(json.description);
                $("#price").html(json.price);
     
                $("#details_read").show();
                $("#read_modal").dialog({
                    width: 850, //<!-- ------------- ancho de la ventana -->
                    height: 500, //<!--  ------------- altura de la ventana -->
                    //show: "scale", <!-- ----------- animación de la ventana al aparecer -->
                    //hide: "scale", <!-- ----------- animación al cerrar la ventana -->
                    resizable: "true", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                    //position: "down",<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                    modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                    buttons: {
                        Ok: function () {
                            $(this).dialog("close");
                        }
                    },
                    show: {
                        effect: "blind",
                        duration: 1000
                    },
                    hide: {
                        effect: "explode",
                        duration: 500
                    }
                });
            }//end-else
        });
    }); 

});