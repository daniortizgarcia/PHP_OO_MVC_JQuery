<div id="contenido">
    <div class="container">
		<h3 data-tr="Lista de Zapatos"></h3>
		<p id="shoes_create"><a href="index.php?page=controller_shoes&op=create" data-tr="Crear Zapatos"></a></p>
        <!-- <p id="shoes_create"><a href="index.php?page=controller_shoes&op=dummies" data-tr=" Zapatos"></a></p> -->
        <?php
            if ($rlt->num_rows === 0){
                echo '<tr>';
                echo '<td align="center"  colspan="3">No se ha creado ninguna zapatilla</td>';
                echo '</tr>';
            }else{
                foreach ($rlt as $row) {
               		echo '<div class="shoes_container">';
                	   	echo '<div class="shoes_cref" class="shoes_info">'. $row['cref'] . '</div>';
                	   	echo '<div class="shoes_info">'. $row['brand'] . '</div>';
                	   	echo '<div class="shoes_info">'. $row['model'] . '</div>';
                	   	echo '<div class="shoes_buttons">';
                        echo '<a class="read_modal" id="' . $row['cref'] . '">Read</a>';
                	   	echo '<a href="index.php?page=controller_shoes&op=update&id='.$row['cref'].'">Update</a>';
                	   	echo '&nbsp;';
                	   	echo '<a href="index.php?page=controller_shoes&op=delete&id='.$row['cref'].'">Delete</a>';
                	   	echo '</div>';
            	   	echo '</div>';
                }
            }
        ?>
    </div>
</div>

<!-- modal window -->
<section id="read_modal" title="Read">
    <div id="details_read" style="display: none;">
        Reference Code: <div id="cref"></div></br>
        Brand: <div id="brand"></div></br>
        Model: <div id="model"></div></br>
        Mail: <div id="mail"></div></br>
        Sole Type: <div id="t_sole"></div></br>
        Size: <div id="size"></div></br>
        Start promotion: <div id="datep"></div></br>
        End Promotion: <div id="datep2"></div></br>
        Terrain: <div id="terrain"></div></br>
        Description: <div id="description"></div></br>
        Price: <div id="price"></div></br>
    </div>
</section>























