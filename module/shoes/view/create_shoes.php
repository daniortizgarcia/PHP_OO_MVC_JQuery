<div id="form">
<form method="post" name="formshoes" id="formshoes">


	<h1 data-tr="Registrar Zapatillas"></h1>
	<?php
	$error = $valide['error'];
	if(isset($error['cref'])){
		print ("<BR><span CLASS='styerror'>" . "* ". $error['cref'] . "</span><br/>");
	}?>
	<br>
	<div class="bodyform">
		<label data-tr="Código de referencia:"> </label><br><input type="text" name="cref" class="cref" value="<?php echo $_POST?$_POST['cref']:""; ?>">
		<br><span id="e_cref" class="styerror"></span>

	</div>
	<br>
	<?php
	if(isset($error['brand'])){
		print ("<BR><span CLASS='styerror'>" . "* ". $error['brand'] . "</span><br/>");
	}?>
	<div class="bodyform">
		<label data-tr="Marca:"> </label><br><input type="text" name="brand" class="brand" value="<?php echo $_POST?$_POST['brand']:""; ?>">
		<br><span id="e_brand" class="styerror"></span>
	</div>
	<br>
	<?php
	if(isset($error['model'])){
		print ("<BR><span CLASS='styerror'>" . "* ". $error['model'] . "</span><br/>");
	}?>
	<div class="bodyform">
		<label data-tr="Modelo:"> </label><br><input type="text" name="model" class="model" value="<?php echo $_POST?$_POST['model']:""; ?>">
		<br><span id="e_model" class="styerror"></span>
	</div>
	<br>
	<?php
	if(isset($error['mail'])){
		print ("<BR><span CLASS='styerror'>" . "* ". $error['mail'] . "</span><br/>");
	}?>
	<div class="bodyform">
		<label data-tr="Correo:"> </label><br><input type="text" name="mail" class="mail" value="<?php echo $_POST?$_POST['mail']:""; ?>">
		<br><span id="e_mail" class="styerror"></span>
	</div>
	<br>
	<div><label data-tr="Tipos de suela:"> </label>
		<select name="t_sole" class="sole" required="">
	        <?php
				$s_sole = array('Flat', 'Cleats', 'MiniCleats');
				for ($i= 0; $i < count($s_sole); $i++) { 
					if($s_sole[$i] == $_POST['t_sole'])
						echo "$s_sole[$i] <option value='$s_sole[$i]' selected>$s_sole[$i]</option> ";
					else
						echo "$s_sole[$i] <option value='$s_sole[$i]'>$s_sole[$i]</option> ";
				}
			?>
	 	</select>
	</div>
	<br>
	<div><label data-tr="Talla:"> </label>
		<?php
			$a_size = array('33','34','35','36','37','38','39','40','41','42','43','44','45','46');
		if($_POST){
			for ($i= 0; $i < count($a_size); $i++) { 
				if($a_size[$i] == $_POST['size'])
					echo "$a_size[$i] <input type='radio' name='size' value='$a_size[$i]' checked> ";
				else
					echo "$a_size[$i] <input type='radio' name='size' value='$a_size[$i]'> ";
			}
		}else{
			for ($i= 0; $i < count($a_size); $i++) { 
				if($i == 0)
					echo "$a_size[$i] <input type='radio' name='size' value='$a_size[$i]' checked> ";
				else
					echo "$a_size[$i] <input type='radio' name='size' value='$a_size[$i]'> ";
			}
		}
		?>	
	</div>
	<br>
	<?php
	if(isset($error['datep'])){
		print ("<BR><span CLASS='styerror'>" . "* ". $error['datep'] . "</span><br/>");
	}?>
	<div class="bodyform">
		<label data-tr="Empezar promoción:"> </label><br><input type="text" class="datep" name="datep" value="<?php echo $_POST?$_POST['datep']:""; ?>">
		<br><span id="e_datep" class="styerror"></span>
	</div class="bodyform">
	<br>
	<?php
	if(isset($error['datep2'])){
		print ("<BR><span CLASS='styerror'>" . "* ". $error['datep2'] . "</span><br/>");
	}?>
	<div class="bodyform">
		<label data-tr="Finalizar promoción:"> </label><br><input type="text" class="datep2" name="datep2" value="<?php echo $_POST?$_POST['datep2']:""; ?>">
		<br><span id="e_datep2" class="styerror"></span>
	</div>
	<br>
	<?php
	if(isset($error['description'])){
		print ("<BR><span CLASS='styerror'>" . "* ". $error['description'] . "</span><br/>");
	}?>
	<div class="bodyform">
		<label data-tr="Descripción:"> </label><br><textarea name="description" class="description"><?php echo $_POST?$_POST['description']:""; ?></textarea>
		<br><span id="e_description" class="styerror"></span>
	</div>
	<br>
	<div><label data-tr="Terreno:"> </label>
		<?php
			$a_terrain = array('Artificial Grass', 'Natural Grass', 'Cement', 'Parquet');

			if ($_POST) {
				for ($i= 0; $i < count($a_terrain); $i++) {
                    if (in_array($a_terrain[$i], $_POST['terrain']))
                        echo "$a_terrain[$i] <input type='checkbox' id='terrain[]' name='terrain[]' value='$a_terrain[$i]' checked>";
                    else
                        echo "$a_terrain[$i] <input type='checkbox' id='terrain[]' name='terrain[]' value='$a_terrain[$i]'>";
               	}
			}else{
				for ($i= 0; $i < count($a_terrain); $i++) { 
					if ($i == 0) 
						echo "$a_terrain[$i] <input type='checkbox' name='terrain[]' value='$a_terrain[$i]' checked>";
					else
						echo "$a_terrain[$i] <input type='checkbox' name='terrain[]' value='$a_terrain[$i]'>";
				}          
            }
		?>
		<br><span id="e_terrain" class="styerror"></span>
	</div>
	<br>
	<?php
	if(isset($error['price'])){
		print ("<BR><span CLASS='styerror'>" . "* ". $error['price'] . "</span><br/>");
	}?>
	<div class="bodyform">
		<label data-tr="Precio:"></label><br><input type="text" name="price" class="price" value="<?php echo $_POST?$_POST['price']:""; ?>">
		<br><span id="e_price" class="styerror"></span>
	</div>
	<br><br>
	<input id="buttonform" class="rbutton" name="Submit" type="button" value="Register" onclick="validate_shoes('create')" />
</form>
</div>