<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . '/workspace/miejer/PHP_OO_MVC_JQuery/';
	include($path . "module/shoes/model/validate.php");
	include($path . "module/shoes/model/dummies.php");
    include($path . "module/shoes/model/DAO.php");

@session_start();
if (isset($_SESSION["tiempo"])) {  
	$_SESSION["tiempo"] = time();
}

	//Inicializar errores
	$e_cref = "";
	$e_brand = "";
	$e_model = "";
	$e_mail = "";
	$e_datep = "";
	$e_datep2 = "";
	$e_description = "";
	$e_terrain = "";
	$e_price = "";
	
	switch ($_GET['op']) {

		case 'list':
			try{
				$daoshoes = new DAOshoes();
				$rlt = $daoshoes->select_all_shoes();
			} catch(Exception $e){
				$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
			}

			if(!$rlt){
				$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
			}
			else{
				include("module/shoes/view/list_shoes.php");
			}
			break;

		case 'create':

			$valide = true;

			if ($_POST) {
				
				$valide = validate_shoes("create");

				if($valide['result']){
					try {
						$daoshoes = new DAOshoes();
						$rlt = $daoshoes->insert_shoes($_POST['cref'], $_POST['brand'], $_POST['model'], $_POST['mail'], $_POST['t_sole'], $_POST['size'], $_POST['datep'], $_POST['datep2'], $_POST['description'], $_POST['terrain'],$_POST['price']);
					} catch (Exception $e) {
						$callback = 'index.php?page=503';
			    		die('<script>window.location.href="'.$callback .'";</script>');
					}
					if(!$rlt){
						$callback = 'index.php?page=503';
					    die('<script>window.location.href="'.$callback .'";</script>');
					}
					else{
						echo '<script language="javascript">alert("Registrado en la base de datos correctamente")</script>';
            			$callback = 'index.php?page=controller_shoes&op=list';
        			    die('<script>window.location.href="'.$callback .'";</script>');
					}
				}
			}
			
			include("module/shoes/view/create_shoes.php");
			break;
		case 'update':
			$valide = true;

			if ($_POST) {
				$valide = validate_shoes("update");
				if($valide['result']){
					try{
                        $daoshoes = new DAOshoes();
    		            $rlt = $daoshoes->update_shoes($_POST['cref'], $_POST['brand'], $_POST['model'], $_POST['mail'], $_POST['t_sole'], $_POST['size'], $_POST['datep'], $_POST['datep2'], $_POST['description'], $_POST['terrain'], $_POST['price']);
                    }catch (Exception $e){
                        $callback = 'index.php?page=503';
        			    die('<script>window.location.href="'.$callback .'";</script>');
                    }
                    
		            if($rlt){
            			echo '<script language="javascript">alert("Actualizado en la base de datos correctamente")</script>';
            			$callback = 'index.php?page=controller_shoes&op=list';
        			    die('<script>window.location.href="'.$callback .'";</script>');
            		}else{
            			$callback = 'index.php?page=503';
    			        die('<script>window.location.href="'.$callback .'";</script>');
            		}
				}
			}

			try {
				$daoshoes = new DAOshoes();
				$rlt = $daoshoes->select_shoes($_GET['id']);
				$shoes=get_object_vars($rlt);
				
			} catch (Exception $e) {
				$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
			}
			if(!$rlt){
				$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
			}
			else{
				
				include("module/shoes/view/update_shoes.php");
			}

			break;

		case 'delete':
			if (isset($_POST['delete'])){
				try{
					$daoshoes = new DAOshoes();
					$rlt = $daoshoes->delete_shoes($_GET['id']);
				} catch (Exception $e){
					$callback = 'index.php?page=503';
				    die('<script>window.location.href="'.$callback .'";</script>');
				}
				if(!$rlt){
					$callback = 'index.php?page=503';
				    die('<script>window.location.href="'.$callback .'";</script>');
				}
				else{
					echo '<script language="javascript">alert("Borrado en la base de datos correctamente")</script>';
	        		$callback = 'index.php?page=controller_shoes&op=list';
	    			die('<script>window.location.href="'.$callback .'";</script>');
				}
			}
			try {
				$daoshoes = new DAOshoes();
				$rlt = $daoshoes->select_shoes($_GET['id']);
				$shoes=get_object_vars($rlt);
			} catch (Exception $e) {
				$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
			}
			if(!$rlt){
				$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
			}
			else{
				include("module/shoes/view/delete_shoes.php");
			}
			break;

		/*case 'dummies':
			if (isset($_POST['dummies'])){
				try {
					$rdumm = load_dummies();
				} catch (Exception $e) {
					$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
				}
				if(!$rdumm){
					$callback = 'index.php?page=503';
			   	 die('<script>window.location.href="'.$callback .'";</script>');
				}else{
					echo '<script language="javascript">alert("Creado en la base de datos correctamente")</script>';
        			$callback = 'index.php?page=controller_shoes&op=list';
    				die('<script>window.location.href="'.$callback .'";</script>');;
				}
				
			}
			include("module/shoes/view/list_dummies.php");
			break;*/
		case 'read_modal':

            try{
                $daoshoes = new DAOshoes();
            	$rlt = $daoshoes->select_shoes($_GET['modal']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rlt){
    			echo json_encode("error");
                exit;
    		}else{
    		    $shoes=get_object_vars($rlt);
                echo json_encode($shoes);
                exit;
    		}
            break;
		default:
			include("view/inc/error404.php");
			break;
	}