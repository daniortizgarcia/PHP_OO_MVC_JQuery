<?php
    /*error_reporting(E_ALL);
    ini_set('display_errors', '1');*/
    session_start();
	if((isset($_GET['page'])) && ($_GET['page'] === "controller_shoes") )
		include("view/inc/top_page_shoes.php");
    elseif((isset($_GET['page'])) && ($_GET['page'] === "controller_order") )
        include("view/inc/top_page_order.php");
    elseif((isset($_GET['page'])) && ($_GET['page'] === "controller_contact") )
        include("view/inc/top_page_contact.php");
	else
		include("view/inc/top_page.php");

	
?>

<div id="wrapper">	
    <div>
        <?php 
            if (isset($_SESSION['type'])) {
                if ($_SESSION['type'] === "Admin") {
                    include("view/inc/menu_admin.php");
                }elseif ($_SESSION['type'] === "Client") {
                    include("view/inc/menu_user.php");
                 }
                else{
                    include("view/inc/menu.php");
                }
            }
            else{
                include("view/inc/menu.php");
            }
         ?>
    </div>  	
    <div>    	
    	<?php include("view/inc/header.php"); ?>        
    </div>  
    <div>
    	<?php include("view/inc/pages.php"); ?>        
        <br style="clear:both;" />
    </div>
    <div>   	   
	    <?php include("view/inc/footer.php"); ?>        
    </div>
</div>
<?php include("view/inc/bottom_page.php"); ?>

