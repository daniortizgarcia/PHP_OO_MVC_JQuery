-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 12-03-2018 a las 18:37:10
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `shop_shoes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dummies`
--

CREATE TABLE `dummies` (
  `cbrand` varchar(50) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `img` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `dummies`
--

INSERT INTO `dummies` (`cbrand`, `brand`, `img`) VALUES
('08649-a', 'New Balance', 'view/img/newbalance.jpg'),
('12345-a', 'Adidas', 'view/img/adidas.jpg'),
('18745-b', 'New balance', 'view/img/newbalancet.jpg'),
('23256-a', 'Munich', 'view/img/munich.jpg'),
('37428-b', 'Adidas', 'view/img/adidast.jpg'),
('45867-a', 'Joma', 'view/img/joma.jpg'),
('49764-b', 'Munich', 'view/img/municht.jpg'),
('63212-b', 'Nike', 'view/img/niket.jpg'),
('82452-a', 'Nike', 'view/img/nike.jpg'),
('85328-b', 'Joma', 'view/img/jomat.jpg'),
('87434-a', 'Kelme', 'view/img/kelme.jpg'),
('96245-b', 'Puma', 'view/img/pumat.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favorite`
--

CREATE TABLE `favorite` (
  `idfav` int(4) NOT NULL,
  `cref` varchar(60) DEFAULT NULL,
  `client` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `favorite`
--

INSERT INTO `favorite` (`idfav`, `cref`, `client`) VALUES
(77, '64268-n', 'Dani'),
(78, '94624-u', 'Dani'),
(79, '64268-n', ''),
(80, '64268-n', ''),
(81, '12345-g', 'Dani');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fcontact`
--

CREATE TABLE `fcontact` (
  `IDcontact` int(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `phone` varchar(75) NOT NULL,
  `message` varchar(200) NOT NULL,
  `hora` varchar(50) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fcontact`
--

INSERT INTO `fcontact` (`IDcontact`, `name`, `surname`, `mail`, `phone`, `message`, `hora`, `fecha`) VALUES
(7, 'Dani', 'Ortiz Garcia', 'danipuerta54@gmail.com', '653378471', 'aaaaaaaaaaaaaaaaa', '2018-01-03 09:51:14', '2018-01-03'),
(8, 'Dani', 'Ortiz Garcia', 'danipuerta54@gmail.com', '653378471', 'aaaaaaaaaaaaaaaaaaaaaaaaaa', '2018-01-03 10:58:35', '2018-01-03'),
(9, 'Dani', 'Ortiz Garcia', 'danipuerta54@gmail.com', '653378471', 'aaaaaaaaaaaaaaaaaaaaaaaaaa', '2018-01-05 18:14:50', '2018-01-05'),
(10, 'Dani', 'Ortiz Garcia', 'danipuerta54@gmail.com', '653378471', 'aaaaaaaaaaaaaaaaaa', '2018-01-13 19:10:29', '2018-01-13'),
(11, 'Dani', 'Ortiz Garcia', 'danipuerta54@gmail.com', '653378471', 'aaaaaaaaaaaaaaaaaaaa', '2018-01-14 11:48:07', '2018-01-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `purchases`
--

CREATE TABLE `purchases` (
  `idpurchases` int(4) NOT NULL,
  `cref` varchar(60) DEFAULT NULL,
  `client` varchar(40) DEFAULT NULL,
  `qty` varchar(40) DEFAULT NULL,
  `price` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `purchases`
--

INSERT INTO `purchases` (`idpurchases`, `cref`, `client`, `qty`, `price`) VALUES
(9, '64893-q', 'Dani', '1', '46'),
(17, '20199-b', 'Aleja', '2', '144'),
(18, '33591-w', 'Aleja', '1', '34'),
(19, '84347-a', 'Aleja', '1', '42'),
(20, '08900-u', 'Aleja', '1', '43'),
(22, '20929-m', 'Enric', '3', '441'),
(23, '71681-b', 'Enric', '1', '46'),
(26, '64268-n', 'Enric', '2', '206'),
(29, '84347-a', 'sergi', '2', '84'),
(30, '33591-w', 'sergi', '2', '68'),
(32, '12345-g', 'Carlos', '1', '150'),
(34, '12345-g', 'Dani', '1', '150'),
(35, '55476-k', 'Dani', '1', '40'),
(37, '12345-x', 'Dani', '2', '198'),
(38, '84347-a', 'Dani', '1', '42'),
(39, '12345-b', 'Dani', '2', '240'),
(40, '84347-a', 'Dani', '1', '42'),
(41, '12345-g', 'Dani', '2', '300'),
(42, '12345-x', 'Dani', '2', '198'),
(45, '12345-x', 'Dani', '1', '99'),
(46, '64268-n', 'Dani', '1', '103'),
(47, '12345-g', 'Dani', '1', '150'),
(49, '12345-b', 'Dani', '2', '240'),
(50, '12345-x', 'Dani', '1', '99'),
(51, '84347-a', 'Dani', '1', '42'),
(52, '12345-x', 'Dani', '1', '99'),
(53, '84347-a', 'Dani', '1', '42'),
(54, '12345-b', 'Dani', '1', '120'),
(55, '64268-n', 'Dani', '1', '103'),
(56, '84347-a', 'Dani', '1', '42'),
(57, '12345-b', 'Dani', '1', '120'),
(58, '64268-n', 'Dani', '1', '103'),
(59, '12345-g', 'Dani', '1', '150'),
(60, '64268-n', 'Dani', '1', '103'),
(61, '12345-b', 'Dani', '1', '120'),
(62, '12345-b', 'Enric', '1', '120'),
(63, '64268-n', 'Enric', '1', '103'),
(64, '12345-g', 'Ignacio', '1', '150');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shoes`
--

CREATE TABLE `shoes` (
  `cref` varchar(50) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `t_sole` varchar(50) NOT NULL,
  `size` varchar(50) NOT NULL,
  `datep` varchar(100) NOT NULL,
  `datep2` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `terrain` varchar(50) NOT NULL,
  `price` varchar(50) NOT NULL,
  `img` varchar(100) NOT NULL,
  `hora` varchar(60) NOT NULL,
  `fecha` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `shoes`
--

INSERT INTO `shoes` (`cref`, `brand`, `model`, `mail`, `t_sole`, `size`, `datep`, `datep2`, `description`, `terrain`, `price`, `img`, `hora`, `fecha`) VALUES
('07160-u', 'Mizuno', 'Monarcida', 'aleja@gmail.com', 'Flat', '37', '05/05/2018', '06/06/2018', 'Zapatillas muy cÃ³modas con muy buen agarre', 'Parquet,Natural Grass', '33.00', 'view/img/mmonarcidaf.jpg', '2018-01-27 18:44:43', '2018-01-27 18:44:43'),
('08740-b', 'Umbro', 'Medusa', 'pepe@gmail.com', 'Cleats', '39', '01/19/2018', '01/19/2018', 'Mejores zapatillas precio calidad', 'Natural Grass,Cement,Parquet', '120,50', 'view/img/ummedusa.jpg', '2017-12-20 11:46:41', '2017-12-20 11:46:41'),
('08900-u', 'Penalty', 'Matis', 'pepe@gmail.com', 'MiniCleats', '43', '05/05/2018', '06/06/2018', 'Las mejores zapatillas para un delantero de verdad', 'Natural Grass,Cement', '43,00', 'view/img/penaltymatisf.webp', '2018-01-27 18:43:40', '2018-01-27 18:43:40'),
('11544-x', 'Munich', 'Gresca', 'pepe@gmail.com', 'Flat', '37', '05/05/2018', '06/06/2018', 'Nunca estarÃ¡s mas cÃ³modo que con estas zapatillas', 'Artificial Grass,Natural Grass,Cement,Parquet', '36.00', 'view/img/mugrescaf.jpg', '2018-01-27 18:43:52', '2018-01-27 18:43:52'),
('12345-b', 'Adidas', 'Nemeziz 17', 'danipuerta54@gmail.com', 'Flat', '38', '01/24/2018', '01/24/2018', 'Zapatillas muy cï¿½modas con muy buen agarre', 'Artificial Grass', '120,00', 'view/img/anemeziz_17f.png', '2018-01-14 12:12:01', '2018-01-14 12:12:01'),
('12345-g', 'Adidas', 'Ace 17', 'danipuerta54@gmail.com', 'Cleats', '38', '03/08/2018', '03/08/2018', 'Son el modelo mas nuevo, con la mejor tecnologï¿½a', 'Artificial Grass', '150,00', 'view/img/aace17.jpg', '2018-01-08 19:42:45', '2018-01-08 19:42:45'),
('12345-h', 'Nike', 'Magista', 'danipuerta54@gmail.com', 'Cleats', '41', '01/12/2018', '01/12/2018', 'Nunca estaras mas comodo que con estas zapatillas', 'Artificial Grass,Natural Grass', '50,00', 'view/img/nimagista.jpg', '2018-01-11 12:34:07', '2018-01-11 12:34:07'),
('12345-k', 'Nike', 'Mercurial', 'danipuerta54@gmail.com', 'Cleats', '38', '01/09/2018', '01/10/2018', 'Zapatillas muy comodas con muy buen agarre', 'Artificial Grass,Natural Grass', '34,99', 'view/img/nimercurial.jpg', '2018-01-08 12:46:31', '2018-01-08 12:46:31'),
('12345-x', 'Adidas', 'Predator', 'danipuerta54@gmail.com', 'Cleats', '39', '01/12/2018', '01/12/2018', 'La mejores zapatillas que puedes encontrar en todo el mercado', 'Artificial Grass', '99,99', 'view/img/apredator.jpg', '2018-01-11 12:37:23', '2018-01-11 12:37:23'),
('20199-b', 'Mizuno', 'Sala premium', 'manolo@gmail.com', 'Flat', '42', '05/05/2018', '06/06/2018', 'Nunca estarÃ¡s mas cÃ³modo que con estas zapatillas', 'Artificial Grass,Natural Grass', '72', 'view/img/msalapremiumf.webp', '2018-01-27 19:01:58', '2018-01-27 19:01:58'),
('20929-m', 'Lotto', 'Spider', 'aleja@gmail.com', 'Cleats', '35', '05/05/2018', '06/06/2018', 'La mejores zapatillas que puedes encontrar en todo el mercado', 'Natural Grass,Cement', '147', 'view/img/lspider.jpg', '2018-01-27 19:02:10', '2018-01-27 19:02:10'),
('21675-j', 'Munich', 'Continental', 'pepe@gmail.com', 'Flat', '33', '05/05/2018', '06/06/2018', 'La mejores zapatillas que puedes encontrar en todo el mercado', 'Cement,Parquet,Artificial Grass', '46', 'view/img/mucontinentalf.jpg', '2018-01-27 18:43:57', '2018-01-27 18:43:57'),
('33591-w', 'Joma', 'Propulsion', 'pepe@gmail.com', 'Cleats', '36', '05/05/2018', '06/06/2018', 'Nunca estarÃ¡s mas cÃ³modo que con estas zapatillas', 'Natural Grass', '34.00', 'view/img/jpropulsion.jpg', '2018-01-27 18:44:20', '2018-01-27 18:44:20'),
('38202-e', 'Puma', 'EvoSpeed', 'dani@gmail.com', 'Cleats', '34', '05/05/2018', '06/06/2018', 'Mejores zapatillas precio calidad', 'Natural Grass,Artificial Grass', '45', 'view/img/puevospeed.jpg', '2018-01-27 18:43:33', '2018-01-27 18:43:33'),
('39556-n', 'Puma', 'Evotouch', 'pepe@gmail.com', 'Cleats', '40', '05/05/2018', '06/06/2018', 'La mejores zapatillas que puedes encontrar en todo el mercado', 'Parquet,Cement,Natural Grass', '44', 'view/img/puevotouch.webp', '2018-01-27 18:52:58', '2018-01-27 18:52:58'),
('51748-v', 'Puma', 'EvoSpeed', 'pepe@gmail.com', 'Flat', '46', '05/05/2018', '06/06/2018', 'Las mejores zapatillas para un delantero de verdad', 'Natural Grass', '125', 'view/img/puevospeedf.jpg', '2018-01-27 19:02:19', '2018-01-27 19:02:19'),
('55476-k', 'Lotto', 'Zhero gravity 700', 'dani@gmail.com', 'Cleats', '42', '05/05/2018', '06/06/2018', 'Nunca estarÃ¡s mas cÃ³modo que con estas zapatillas', 'Natural Grass', '40', 'view/img/lzherogravity.jpg', '2018-01-27 18:44:29', '2018-01-27 18:44:29'),
('56753-n', 'Nike', 'Hypervenom', 'esteban@gmail.com', 'Flat', '33', '05/05/2018', '06/06/2018', 'Son el modelo mas nuevo, con la mejor tecnologÃ­a', 'Cement', '100.00', 'view/img/nihypervenom.jpg', '2018-01-27 18:40:45', '2018-01-27 18:40:45'),
('64268-n', 'Adidas', 'Nemeziz 17', 'dani@gmail.com', 'Cleats', '46', '05/05/2018', '06/06/2018', 'Las mejores zapatillas para un delantero de verdad', 'Parquet,Natural Grass,Cement', '103', 'view/img/nemeziz17.png', '2018-01-27 19:03:01', '2018-01-27 19:03:01'),
('64893-q', 'Munich', 'Mundial', 'pepe@gmail.com', 'Cleats', '35', '05/05/2018', '06/06/2018', 'Son el modelo mas nuevo, con la mejor tecnologÃ­a', 'Natural Grass,Parquet', '46', 'view/img/mumundial.jpg', '2018-01-27 18:43:26', '2018-01-27 18:43:26'),
('68773-n', 'Penalty', 'Commander', 'manolo@gmail.com', 'MiniCleats', '46', '05/05/2018', '06/06/2018', 'Las mejores zapatillas para un delantero de verdad', 'Artificial Grass,Cement', '34.00', 'view/img/penaltycommandermc.jpg', '2018-01-27 18:43:46', '2018-01-27 18:43:46'),
('71681-b', 'Kipsta', 'CLR300', 'manolo@gmail.com', 'Cleats', '33', '05/05/2018', '06/06/2018', 'Nunca estarÃ¡s mas cÃ³modo que con estas zapatillas', 'Natural Grass,Parquet,Cement', '46', 'view/img/kiclr300.jpeg', '2018-01-27 18:44:36', '2018-01-27 18:44:36'),
('81111-n', 'Kelme', 'Feline', 'adri@gmail.com', 'Flat', '40', '05/05/2018', '06/06/2018', 'La mejores zapatillas que puedes encontrar en todo el mercado', 'Cement,Natural Grass', '133', 'view/img/kfelinef.jpg', '2018-01-27 19:03:16', '2018-01-27 19:03:16'),
('84347-a', 'Adidas', 'Predator', 'aleja@gmail.com', 'MiniCleats', '40', '05/05/2018', '06/06/2018', 'Las mejores zapatillas para un delantero de verdad', 'Cement,Natural Grass,Parquet', '42', 'view/img/apredatormc.jpeg', '2018-01-27 19:03:11', '2018-01-27 19:03:11'),
('94624-u', 'Mizuno', 'Monarcida', 'dani@gmail.com', 'MiniCleats', '38', '05/05/2018', '06/06/2018', 'Nunca estarÃ¡s mas cÃ³modo que con estas zapatillas', 'Artificial Grass,Parquet,Natural Grass', '57', 'view/img/mmonarcidamc.jpg', '2018-01-27 19:02:15', '2018-01-27 19:02:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `user` varchar(50) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`name`, `surname`, `user`, `mail`, `password`, `type`) VALUES
('Adrian', 'Gramaje', 'Adri', 'adri@gmail.com', '$1$rasmusle$0GYWlU.VOlg2Kw0dULQZb1', 'Client'),
('Alejandro', 'Pla', 'Aleja', 'aleja@gmail.com', '$1$rasmusle$0GYWlU.VOlg2Kw0dULQZb1', 'Client'),
('Carlos', 'Colomer', 'Carlos', 'carlos@gmail.com', '$1$rasmusle$0GYWlU.VOlg2Kw0dULQZb1', 'Client'),
('Dani', 'Ortiz', 'Dani', 'danipuerta54@gmail.com', '$1$rasmusle$0GYWlU.VOlg2Kw0dULQZb1', 'Admin'),
('Enric', 'Alcaraz', 'Enric', 'enric@gmail.com', '$1$rasmusle$0GYWlU.VOlg2Kw0dULQZb1', 'Client'),
('Ignacio', 'Gisbert', 'Ignacio', 'ignacio@gmail.com', '$1$rasmusle$0GYWlU.VOlg2Kw0dULQZb1', 'Admin'),
('Juanan', 'Gisbert', 'Juanan', 'juanan@gmail.com', '$1$rasmusle$0GYWlU.VOlg2Kw0dULQZb1', 'Client'),
('sergi', 'chafer', 'sergi', 'sergi@gmail.com', '$1$kpGwp3fa$C5SWLqPr653ej8c.oR2dj1', 'Client');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `dummies`
--
ALTER TABLE `dummies`
  ADD PRIMARY KEY (`cbrand`);

--
-- Indices de la tabla `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`idfav`),
  ADD KEY `cref` (`cref`),
  ADD KEY `client` (`client`);

--
-- Indices de la tabla `fcontact`
--
ALTER TABLE `fcontact`
  ADD PRIMARY KEY (`IDcontact`);

--
-- Indices de la tabla `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`idpurchases`),
  ADD KEY `cref` (`cref`),
  ADD KEY `client` (`client`);

--
-- Indices de la tabla `shoes`
--
ALTER TABLE `shoes`
  ADD PRIMARY KEY (`cref`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `favorite`
--
ALTER TABLE `favorite`
  MODIFY `idfav` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT de la tabla `fcontact`
--
ALTER TABLE `fcontact`
  MODIFY `IDcontact` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `purchases`
--
ALTER TABLE `purchases`
  MODIFY `idpurchases` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `purchases_ibfk_1` FOREIGN KEY (`cref`) REFERENCES `shoes` (`cref`),
  ADD CONSTRAINT `purchases_ibfk_2` FOREIGN KEY (`client`) REFERENCES `users` (`user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
