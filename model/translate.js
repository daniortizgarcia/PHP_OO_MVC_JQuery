frases = {
  "en": {
    "Cambiar idioma:": "Change language:",
    "Español": "Spanish",
    "Inglés": "English",
    "Valenciano": "Valencian",
    "Marcas que puedes encontrar": "Brands you can find",
    "Fútbol": "Football",
    "Fútbol Sala": "Indoor Football",
    "Inicio": "Home",
    "Zapatos": "Shoes",
    "Sobre Nosotros": "About us",
    "Contactanos": "Contact us",
    "Servicios": "Services",
    "Ultimas Novedades" : "Latest News",
    "Ordenar" : "Order",
    "Lista de Zapatos" : "Shoes List",
    "Sobre Nosotros" : "About Us",
    "Quienes somos" : "What we are",
    "Somos una tienda que en su mayor parte vende zapatillas, tambien podeís comprar complementos como: balones, camisetas, pantalones deportivos, guntes de portero, espinilleras y mucho mas. La tienda oficial esta ubicada en españa. Si quereis contactar con nosotros podeís utilizar el forumulario de contacto que tendreis en el aprtado de contactanos." : "We are a store that mostly sells sneakers, you can also buy accessories such as: balls, T-shirts, sports pants, porters, shin guards and much more. The official shop is located in Spain. If you want to contact us you can use the contact forum that you will have in the contact form.",
    "Filtrar Zapatillas":"Filter Shoes",
    "Crear Zapatos" : "Create Shoes",
    "Código de referencia:" : "Reference code:",
    "Marca:" : "Brand:",
    "Modelo:" : "Model:",
    "Correo:" : "Mail:",
    "Tipos de suela:" : "Sole Type:",
    "Talla:" : "Size:",
    "Empezar promoción:" : "Start Promotion:",
    "Finalizar promoción:" : "End Promotion:",
    "Descripción:" : "Description:",
    "Terreno:" : "Terrain:",
    "Precio:" : "Price:",
    "Registrar Zapatillas" : "Register Shoes",
    "Nuestras marcas" : "Our brands",
    "Patrocinios" : "Sponsorships",
    "El mayor patrocinador de nuestra categoria de futbol sala, la lnfs es la liga nacional de futbol sala de españa, los equipos mas famosos son el inter movistar, el pozo murcia y el barcelona. Unos de los patrocinios tambien de la lnfs es kelme donde podreis encontrar estos productos en nuestra tienda" : "The biggest sponsor of our futsal category, Infs is the national league of Spanish futsal, the most famous teams are the inter movistar, the well murcia and the Barcelona. One of the lnfs sponsors is kelme where you can find these products in our store",
    "El mayor patrocinador de nuestra categoria de futbol español, la liga santander es una de las ligas mas importantes del mundo, ya que tiene a los mejores clubes como el Madrid, Barcelona y El Atletico de Madrid. La pelota oficial de la liga santander la podeis encontrar en nuestra tienda." : "The biggest sponsor of our Spanish football category, the Santander league is one of the most important leagues in the world, as it has the best clubs such as Madrid, Barcelona and El Atletico de Madrid. The official santander league ball can be found in our shop.",
    "El mayor patrocinador de nuestra categoria de futbol ingles, la premier league es una de las ligas mas importantes del mundo, ya que tiene a los mejores clubes como manchester city, manchester united y chelsea. La pelota oficial de la liga santander la podeis encontrar en nuestra tienda." : "The biggest sponsor of our English football category, the Premier League is one of the most important leagues in the world, as it has the best clubs such as Manchester City, Manchester United and Chelsea. The official santander league ball can be found in our shop.",
    "Formulario de Contacto" : "Contact form",  
    "Donde encontrarnos" : "Where to find us",
    "Nombre:" : "Name:",
    "Apellidos:" : "Surname:",
    "Correo electronico:" : "email addres:",
    "Teléfono:" : "Phone:",
    "Mensaje:" : "Message:",
    "Tarifa Mensual" : "Monthly Rate",
    "- Se le cobrara 10€ al mes" : "You will be charged 10€ per month",
    "- Tendra envios premium gratis" : "Get free premium shipping",
    "- Participara en los sorteos mensuales" : "Participate in monthly draws",
    "Tarifa Anual" : "Annual Rate",
    "- Se le cobrara 20€ al año" : "You will be charged 20€ per year",
    "- Tendra envios standar gratis" : "Have free standard shipping",
    "- Participara en el gran sorteo de Navidad" : "Participate in the great Christmas raffle",
    "Tarifa Estandar" : "Standard Rates",
    "- No tiene que pagar nada" : "You don't have to pay anything",
    "- Los envios premium y los standar se le cobraran" : "Premium and standar shipping charges will be charged.",
    "- No podra participar en ningun sorteo" : "You may not participate in any draw.",
    "Envios" : "Shipping",
    "Envio Premium" : "Premium Shipping",
    "- El pedido llega entre 1-2 dias" : "The order arrives within 1-2 days",
    "- Con tarifa mensual es gratis" : "With monthly fee free of charge",
    "- Con tarifa Anual y Estandar son 5€" : "Annual and Standard rates are 5€.",
    "- La empresa encargada de estos envios es Nacex" : "The company in charge of these shipments is Nacex",
    "Envio Estandar" : "Standard Shipping",
    "- El pedido llega entre 3-5 dias" : "The order arrives in 3-5 days",
    "- Con tarifa Mensual y Estandar es gratis" : "Monthly and Standard rates are free of charge.",
    "- Con tarifa Anual son 2,99€" : "Annual fee is 2,99€.",
    "- La empresa encargada de estos envios es Correos" : "The company in charge of these shipments is Correos",
    "Iniciar sesión" : "Log In",
    "Noticias" : "News",
    "Cerrar sesión" : "Log Out",
    "Tienda" : "Shop",
    "Perfil" : "Profile"

  },
  "va": {
    "Cambiar idioma:": "Cambiar idioma:",
    "Español": "Espanyol",
    "Inglés": "Ingles",
    "Valenciano": "Valencia",
    "Marcas que puedes encontrar": "Marques que pots trobar",
    "Fútbol": "Futbol",
    "Fútbol Sala": "Futbol Sala",
    "Inicio": "Inici",
    "Zapatos": "Sabates",
    "Sobre Nosotros": "Sobre Nosaltres",
    "Contactanos": "Contacta'ns",
    "Servicios": "Servicis",
    "Ultimas Novedades" : "Ultimes Novetats",
    "Ordenar" : "Ordenar",
    "Lista de Zapatos" : "Llista de Sabates",
    "Sobre Nosotros" : "Sobre Nosaltres",
    "Quienes somos" : "Qui som",
    "Somos una tienda que en su mayor parte vende zapatillas, tambien podeís comprar complementos como: balones, camisetas, pantalones deportivos, guntes de portero, espinilleras y mucho mas. La tienda oficial esta ubicada en españa. Si quereis contactar con nosotros podeís utilizar el forumulario de contacto que tendreis en el aprtado de contactanos." : "Som una botiga que majoritàriament ven sabatilles, també podeís comprar complements com: balons, camisetes, pantalons esportius, guntes de porter, canyelleres i molt mes. La botiga oficial esta ubicada a espanya. Si voleu contactar amb nosaltres podeís utilitzar el forumulario de contacte que tindreu en l'aprtado de contacta'ns.",
    "Filtrar Zapatillas" : "Filtrar Sabates",
    "Crear Zapatos" : "Crear Sabates",
    "Código de referencia:" : "Códic de referencia:",
    "Marca:" : "Marca:",
    "Modelo:" : "Modelo:",
    "Correo:" : "Correu:",
    "Tipos de suela:" : "Tipo de sola:",
    "Talla:" : "Talla:",
    "Empezar promoción:" : "Comenzar Promoció:",
    "Finalizar promoción:" : "Acabar Promoció:",
    "Descripción:" : "Descripció:",
    "Terreno:" : "Terreny:",
    "Precio:" : "Preu:",
    "Registrar Zapatillas" : "Registrar Sabates",
    "Nuestras marcas" : "Les nostres marques", 
    "Patrocinios" : "Sponsorships",
    "El mayor patrocinador de nuestra categoria de futbol sala, la lnfs es la liga nacional de futbol sala de españa, los equipos mas famosos son el inter movistar, el pozo murcia y el barcelona. Unos de los patrocinios tambien de la lnfs es kelme donde podreis encontrar estos productos en nuestra tienda" : "El mayor patrocinador de nuestra categoria de futbol sala, la lnfs es la liga nacional de futbol sala de españa, los equipos mas famosos son el inter movistar, el pozo murcia y el barcelona. Unos de los patrocinios tambien de la lnfs es kelme donde podreis encontrar estos productos en nuestra tienda",
    "El mayor patrocinador de nuestra categoria de futbol español, la liga santander es una de las ligas mas importantes del mundo, ya que tiene a los mejores clubes como el Madrid, Barcelona y El Atletico de Madrid. La pelota oficial de la liga santander la podeis encontrar en nuestra tienda." : "El major patrocinador de la nostra categoria de futbol espanyol, la lliga santander és una de les lligues mes importants del món, ja que té als millors clubs com el Madrid, Barcelona i L'Atlètic de Madrid. La pilota oficial de la lliga santander la podeu trobar en la nostra botiga.",
    "El mayor patrocinador de nuestra categoria de futbol ingles, la premier league es una de las ligas mas importantes del mundo, ya que tiene a los mejores clubes como manchester city, manchester united y chelsea. La pelota oficial de la liga santander la podeis encontrar en nuestra tienda." : "The biggest espònsor of our English football category, the Premier League is one of the most important leagues in the world, as it has the best clubs such as Manchester City, Manchester United and Chelsea. The official santander league ball ca be found in our shop.",
    "Formulario de Contacto" : "Formulari de Contacte",  
    "Donde encontrarnos" : "Aon trobarmos",
    "Nombre:" : "Nom:",
    "Apellidos:" : "Cognom:",
    "Correo electronico:" : "Correu electronic:",
    "Teléfono:" : "Telefon:",
    "Mensaje:" : "Missatge:",
    "Tarifa Mensual" : "Tarifa Mensual",
    "- Se le cobrara 10€ al mes" : "- Se li cobrara 10€ al mes",
    "- Tendra envios premium gratis" : "- Tindrà enviaments premium gratis",
    "- Participara en los sorteos mensuales" : "- Participara en els sortejos mensuals",
    "Tarifa Anual" : "Tarifa Anual",
    "- Se le cobrara 20€ al año" : "- Se li cobrara 20€ a l'any",
    "- Tendra envios standar gratis" : "- Tindrà enviaments standar gratis",
    "- Participara en el gran sorteo de Navidad" : "- Participara en el gran sorteig de Nadal",
    "Tarifa Estandar" : "Tarifa Estàndard",
    "- No tiene que pagar nada" : "- No ha de pagar res",
    "- Los envios premium y los standar se le cobraran" : "- Els enviaments premium i els standar se li cobraren",
    "- No podra participar en ningun sorteo" : "- No podrà participar en cap sorteig",
    "Envios" : "Enviaments",
    "Envio Premium" : "Enviaments Premium",
    "- El pedido llega entre 1-2 dias" : "- La comanda arriba entre 1-2 dies",
    "- Con tarifa mensual es gratis" : "- Amb tarifa mensual és gratis",
    "- Con tarifa Anual y Estandar son 5€" : "- Amb tarifa Anual i Estàndard són 5€",
    "- La empresa encargada de estos envios es Nacex" : "- L'empresa encarregada d'estos enviaments és Nacex",
    "Envio Estandar" : "Enviament Estàndart",
    "- El pedido llega entre 3-5 dias" : "- La comanda arriba entre 3-5 dies",
    "- Con tarifa Mensual y Estandar es gratis" : "- Amb tarifa Mensual i Estàndard és gratis",
    "- Con tarifa Anual son 2,99€" : "- Amb tarifa Anual són 2,99EUR",
    "- La empresa encargada de estos envios es Correos" : "- L'empresa encarregada d'estos enviaments és Correusss",
    "Iniciar sesión" : "Iniciar sessió",
    "Noticias" : "Notícies",
    "Cerrar sesión" : "Tancar sessió",
    "Tienda" : "Tenda",
    "Perfil" : "Perfil"

  }

};
function cambiarIdioma(lang) {
  // Habilita las 2 siguientes para guardar la preferencia.
  lang = lang || localStorage.getItem('app-lang') || 'es';
  localStorage.setItem('app-lang', lang);

  var elems = document.querySelectorAll('[data-tr]');
  for (var x = 0; x < elems.length; x++) {
    elems[x].innerHTML = frases.hasOwnProperty(lang)
      ? frases[lang][elems[x].dataset.tr]
      : elems[x].dataset.tr;
  }
}
window.onload = function(){
  cambiarIdioma();
  
  document
    .getElementById('btn-es')
    .addEventListener('click', cambiarIdioma.bind(null, 'es'));

  document
    .getElementById('btn-en')
    .addEventListener('click', cambiarIdioma.bind(null, 'en'));

  document
    .getElementById('btn-va')
    .addEventListener('click', cambiarIdioma.bind(null, 'va'));
}
