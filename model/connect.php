<?php
class connect {
		
		public static function con(){
			$host = "127.0.0.1";
			$user = "root";
			$pass = "";
			$db = "shop_shoes";
			$port = 3306;

			$connection = mysqli_connect($host, $user, $pass, $db, $port)or die(mysqli_error());
			return $connection;
		}

		public static function close($connection){
			mysqli_close($connection);
		}
	}